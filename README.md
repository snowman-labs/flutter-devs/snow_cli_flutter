# Flutter Snow Blower! 🚀

<br>
<img src="https://gitlab.com/snowman-labs/flutter-devs/snow_cli_flutter/-/raw/master/images/snow_blower_logo_full.png">
<br><br><br>

<p>
  <a href="https://pub.dev/packages/flutter_snow_blower" target="_blank">
    <img alt="Pub" src="https://img.shields.io/pub/v/flutter_snow_blower?color=orange" />
  </a>
  <img alt="Version" src="https://img.shields.io/badge/version-1.0.0-blue.svg?cacheSeconds=2592000" />
  <a href="https://gitlab.com/snowman-labs/flutter-devs/snow_cli_flutter/-/blob/master/README.md" target="_blank">
    <img alt="Documentation" src="https://img.shields.io/badge/documentation-yes-brightgreen.svg" />
  </a>
  <a href="https://opensource.org/licenses/BSD-3-Clause" target="_blank">
    <img alt="License: BSD-3-Clause" src="https://img.shields.io/badge/BSD-3-Clause" />
  </a>
  <a href="https://twitter.com/SnowmanLabs" target="_blank">
    <img alt="Twitter: SnowmanLabs" src="https://img.shields.io/twitter/follow/SnowmanLabs.svg?style=social" />
  </a>
</p>

🚀 Dart CLI tool to automate your tasks in a flutter project, like:

<img align="right"
src="https://gitlab.com/snowman-labs/flutter-devs/snow_cli_flutter/-/raw/master/images/snow_create_example.gif" height="280">

- Create a project from a tested template
  - Start projects with the following pre-made architectures:
    - [Clean architecture][clean]
    - MVC with [flutter_modular](https://pub.dev/packages/flutter_modular)
  - Today we support only one state management:
    - [Mobx](https://pub.dev/packages/mobx)
- Generate components with your boilerplate like:
  - Pages
  - Widgets
  - Controllers
  - Modules
  - Repositories
  - Use cases (only for clean architecture)
  - Entities (only for clean architecture)
  - Tests
  - And others
- Maintain a pattern in your project
- Install, uninstall or update packages
- Create and run scripts write in pubspec.yaml (run the build_runner for example)

<br>

## 📄 Table of Contents

- **[Acknowledgment](#-acknowledgment)**
- **[Why should I use](#-why-should-i-use)**
- **[Motivations](#-motivations)**
- **[Examples](#-examples)**
- **[Install](#-install)**
- **[Usage](#-usage)**
- **[Commands](#-commands)**
  - [Upgrade](#command-upgrade)
  - [Create](#command-create)
  - [Start](#command-start)
  - [Run](#command-run)
  - [Install](#command-install)
  - [Uninstall](#command-uninstall)
  - [Update](#command-update)
  - [Revert](#command-revert)
  - **[Generate](#command-generate)**
    - [Module](#creates-a-new-module)
    - [Controller](#creates-a-new-controller)
    - [Page](#creates-a-new-page)
    - [Widget](#creates-a-new-widget)
    - [Data source (only Clean architecture)](#creates-a-new-data-source-only-clean-architecture)
    - [Entity (only Clean architecture)](#creates-a-new-entity-only-clean-architecture)
    - [Use case (only Clean architecture)](#creates-a-new-use-case-only-clean-architecture)
    - [I18n](#creates-a-new-i18n-file)
    - [Repository](#create-a-new-repository)
    - [Unit test](#generating-unit-tests)
- **[Run tests](#-run-tests)**
- **[Author](#-author)**
- **[Contributing](#-contributing)**
- **[Show your support](#-show-your-support)**
- **[License](#-license)**

## 👏 Acknowledgment

This package is a fork of [Slidy](https://pub.dev/packages/slidy) package, basically the main difference between is the
performance and mainly the support for [Clean architecture][clean] with some adaptations to works better with the
[Snowman labs](https://snowmanlabs.com) Style! Other new feature is the improvement to the [start](#start) and
[create](#create) command, that you can create a new project with a pre-made login page, register page and home with all
working as it needs to be, the only thing that you need to do is configure the authentication.

## ❓ Why should I use

Snow Blower's goal is to help you to structure your project in a standardized way. Organizing your app in **Modules**
formed by pages, repositories, widgets, controllers, and also create unit tests automatically. The Module gives you a
easier way to inject dependencies and controllers, including automatic dispose. Also helps you installing the dependencies
and packages, updating and removing them. The best is that you can do all of this running a single command.

## 🎯 Motivations

We realized that the project pattern absence is affecting the productivity of most developers, and other problem that had
happening was that the new developers of the project doesn't know the project pattern, and with this tool, all will
create at the same pattern every time. So we're proposing a development pattern along with a tool that imitates NPM (NodeJS)
functionality as well as template generation capabilities.

## 📦 Examples

Examples of to-do apps that used flutter_snow_blower as base:

- [Todo clean](https://gitlab.com/snowman-labs/flutter-devs/project_sample_base)
- [Todo modular](https://gitlab.com/snowman-labs/flutter-devs/todo-app-example-modular)
- [Example clean](https://gitlab.com/snowman-labs/flutter-devs/snow_cli_flutter/-/tree/master/example_clean)
- [Example clean complete](https://gitlab.com/snowman-labs/flutter-devs/snow_cli_flutter/-/tree/master/example_clean_complete)
- [Example MVC modular complete](https://gitlab.com/snowman-labs/flutter-devs/snow_cli_flutter/-/tree/master/example_mvc_modular)
- [Example MVC modular complete](https://gitlab.com/snowman-labs/flutter-devs/snow_cli_flutter/-/tree/master/example_mvc_modular_complete)

## 🔧 Install

1. First of all you need install the Dart:

    [https://dart.dev/get-dart](https://dart.dev/get-dart)

2. Activate the snow using the pub:

```bash
 pub global activate flutter_snow_blower
```

3. Type `snow --version` to make sure everything is working properly. This command should return the installed version.

## 🎉 Usage

```bash
Usage: snow <command> [arguments]

Global options:
-h, --help       Print this usage information.
-v, --version

Available commands:
  create      Create a Flutter project with basic structure
  g           Creates a module, page, widget or repository according to the option.
  generate    Creates a module, page, widget or repository according to the option.
  i           Install (or update) a new package or packages:
  install     Install (or update) a new package or packages:
  revert      Revert last command
  run         run scripts in pubspec.yaml
  start       Create a basic structure for your project (confirm that you have no data in the "lib" folder).
  uninstall   Remove a package
  update      Update a new package or packages.
  upgrade     Upgrade the Flutter Snow Blower CLI version

Run "snow help <command>" for more information about a command.
```

## ✨ Commands

### Command `upgrade`

Upgrades snow version

```bash
snow upgrade
```

### Command `create`

Create a new project with same structure described in start command, but now with the same options of `flutter create`.

```bash
snow create <name-of-project>
```

### Command `start`

Create the basic structure for your project (make sure that your "lib" folder it's empty).
This structure contains the most important things that almost all projects have, like i18n, flavors, a defined state
and architecture...

```bash
snow start
```

You can start a complete project with a template of authentication, login and register page, simply by adding `-c` in the
command:

```bash
snow start -c
```

Now choose in witch architecture do you want use in the project:

![Folder example](https://gitlab.com/snowman-labs/flutter-devs/snow_cli_flutter/-/raw/master/images/choose_architecture.png)

And you will get this Structure depending on which architecture do you selected:

<details>
<summary>Clean architecture</summary>

```dart
example_clean
├─ lib
│  ├─ app
│  │  ├─ app_module.dart
│  │  ├─ app_widget.dart
│  │  ├─ i18n
│  │  │  ├─ app
│  │  │  │  └─ pages
│  │  │  │     └─ home_i18n.dart
│  │  │  └─ validators_i18n.dart
│  │  ├─ pages
│  │  │  └─ home
│  │  │     ├─ home_controller.dart
│  │  │     ├─ home_controller.g.dart
│  │  │     ├─ home_module.dart
│  │  │     └─ home_page.dart
│  │  ├─ styles
│  │  │  ├─ app_bar_theme_app.dart
│  │  │  ├─ app_color_scheme.dart
│  │  │  ├─ app_text_theme.dart
│  │  │  └─ app_theme_data.dart
│  │  ├─ utils
│  │  │  └─ validators.dart
│  │  └─ widgets
│  │     ├─ custom_alert_dialog
│  │     │  ├─ custom_alert_dialog.dart
│  │     │  └─ types
│  │     │     ├─ confirm_dialog.dart
│  │     │     └─ error_dialog.dart
│  │     ├─ example
│  │     │  └─ example_widget.dart
│  │     └─ observer_future
│  │        └─ observer_future.dart
│  ├─ app_flavor_values.dart
│  ├─ data
│  │  ├─ constants
│  │  │  ├─ constants.dart
│  │  │  └─ features.dart
│  │  ├─ data_sources
│  │  │  ├─ feature
│  │  │  │  ├─ feature_local_data_source.dart
│  │  │  │  └─ feature_remote_data_source.dart
│  │  │  └─ todo
│  │  │     ├─ todo_local_data_source.dart
│  │  │     └─ todo_remote_data_source.dart
│  │  ├─ helpers
│  │  │  ├─ error_mapper.dart
│  │  │  └─ extensions.dart
│  │  ├─ local
│  │  │  └─ hive_client.dart
│  │  ├─ mappers
│  │  │  ├─ index.dart
│  │  │  ├─ todo
│  │  │  │  └─ todo_mapper.dart
│  │  │  └─ user_mapper.dart
│  │  ├─ remote
│  │  │  └─ dio_client.dart
│  │  └─ repositories
│  │     └─ feature
│  │        └─ feature_repository_impl.dart
│  ├─ di
│  │  ├─ di.dart
│  │  ├─ di.iconfig.dart
│  │  └─ modules
│  │     ├─ local_module.dart
│  │     └─ remote_module.dart
│  ├─ domain
│  │  ├─ entities
│  │  │  ├─ todo
│  │  │  │  └─ todo_entity.dart
│  │  │  └─ user_entity.dart
│  │  ├─ repositories
│  │  │  └─ feature
│  │  │     └─ feature_repository.dart
│  │  └─ usecases
│  │     ├─ base
│  │     │  ├─ base_future_use_case.dart
│  │     │  ├─ base_stream_use_case.dart
│  │     │  └─ base_use_case.dart
│  │     ├─ entities
│  │     │  └─ user_entity.dart.dart
│  │     ├─ feature
│  │     │  └─ do_something_use_case.dart
│  │     └─ get_todo_list
│  │        └─ get_todo_list_use_case.dart
│  ├─ main_dev.dart
│  ├─ main_production.dart
│  └─ main_qa.dart
└─ test
   ├─ app
   │  ├─ i18n
   │  │  └─ app
   │  │     └─ pages
   │  │        └─ home_i18n_test.dart
   │  └─ pages
   │     └─ home
   │        ├─ home_controller_test.dart
   │        └─ home_page_test.dart
   ├─ data
   │  └─ remote
   │     └─ dio_client_test.dart
   ├─ drawer_features_example_test.dart
   └─ test_variants
      └─ flavors_test_variant.dart
```

</details>
<details>
<summary>Clean architecture <b>(complete mode)</b></summary>

```dart
example_clean_complete
├─ lib
│  ├─ app
│  │  ├─ app_module.dart
│  │  ├─ app_widget.dart
│  │  ├─ i18n
│  │  │  ├─ app
│  │  │  │  └─ pages
│  │  │  │     └─ home_i18n.dart
│  │  │  ├─ login_i18n.dart
│  │  │  └─ validators_i18n.dart
│  │  ├─ pages
│  │  │  ├─ home
│  │  │  │  ├─ home_controller.dart
│  │  │  │  ├─ home_controller.g.dart
│  │  │  │  ├─ home_module.dart
│  │  │  │  └─ home_page.dart
│  │  │  ├─ login
│  │  │  │  ├─ login_module.dart
│  │  │  │  └─ pages
│  │  │  │     ├─ login
│  │  │  │     │  ├─ login_controller.dart
│  │  │  │     │  ├─ login_controller.g.dart
│  │  │  │     │  └─ login_page.dart
│  │  │  │     └─ register
│  │  │  │        ├─ register_controller.dart
│  │  │  │        ├─ register_controller.g.dart
│  │  │  │        └─ register_page.dart
│  │  │  └─ splash
│  │  │     ├─ splash_controller.dart
│  │  │     ├─ splash_controller.g.dart
│  │  │     ├─ splash_module.dart
│  │  │     └─ splash_page.dart
│  │  ├─ stores
│  │  │  └─ auth
│  │  │     ├─ auth_store.dart
│  │  │     └─ auth_store.g.dart
│  │  ├─ styles
│  │  │  ├─ app_bar_theme_app.dart
│  │  │  ├─ app_color_scheme.dart
│  │  │  ├─ app_text_theme.dart
│  │  │  └─ app_theme_data.dart
│  │  ├─ utils
│  │  │  └─ validators.dart
│  │  └─ widgets
│  │     ├─ custom_alert_dialog
│  │     │  ├─ custom_alert_dialog.dart
│  │     │  └─ types
│  │     │     ├─ confirm_dialog.dart
│  │     │     └─ error_dialog.dart
│  │     ├─ login
│  │     │  ├─ email_text_field_widget.dart
│  │     │  └─ password_text_field_widget.dart
│  │     └─ observer_future
│  │        └─ observer_future.dart
│  ├─ app_flavor_values.dart
│  ├─ data
│  │  ├─ constants
│  │  │  ├─ constants.dart
│  │  │  └─ features.dart
│  │  ├─ data_sources
│  │  │  └─ auth
│  │  │     ├─ auth_local_data_source.dart
│  │  │     └─ auth_remote_data_source.dart
│  │  ├─ helpers
│  │  │  ├─ error_mapper.dart
│  │  │  └─ extensions.dart
│  │  ├─ local
│  │  │  └─ hive_client.dart
│  │  ├─ mappers
│  │  │  ├─ auth_mapper.dart
│  │  │  ├─ index.dart
│  │  │  └─ user_mapper.dart
│  │  ├─ remote
│  │  │  └─ dio_client.dart
│  │  └─ repositories
│  │     └─ auth
│  │        └─ auth_repository_impl.dart
│  ├─ di
│  │  ├─ di.dart
│  │  ├─ di.iconfig.dart
│  │  └─ modules
│  │     ├─ local_module.dart
│  │     └─ remote_module.dart
│  ├─ domain
│  │  ├─ entities
│  │  │  ├─ auth_entity.dart
│  │  │  ├─ auth_status.dart
│  │  │  └─ user_entity.dart
│  │  ├─ repositories
│  │  │  └─ auth
│  │  │     └─ auth_repository.dart
│  │  └─ usecases
│  │     ├─ auth
│  │     │  ├─ get_auth_status_stream_use_case.dart
│  │     │  ├─ get_user_stream_use_case.dart
│  │     │  ├─ get_user_use_case.dart
│  │     │  ├─ login_user_email_use_case.dart
│  │     │  ├─ logout_user_use_case.dart
│  │     │  └─ register_user_email_use_case.dart
│  │     └─ base
│  │        ├─ base_future_use_case.dart
│  │        ├─ base_stream_use_case.dart
│  │        └─ base_use_case.dart
│  ├─ main_dev.dart
│  ├─ main_production.dart
│  └─ main_qa.dart
└─ test
   ├─ app
   │  ├─ i18n
   │  │  ├─ app
   │  │  │  └─ pages
   │  │  │     └─ home_i18n_test.dart
   │  │  └─ i18n_test.dart
   │  └─ pages
   │     └─ home
   │        ├─ home_controller_test.dart
   │        └─ home_page_test.dart
   ├─ data
   │  └─ remote
   │     └─ dio_client_test.dart
   ├─ drawer_features_example_test.dart
   └─ test_variants
      └─ flavors_test_variant.dart
```

</details>

<details>
<summary>MVC with modular</summary>

```dart
example_mvc_modular
├─ lib
│  ├─ app_flavor_values.dart
│  ├─ main_dev.dart
│  ├─ main_production.dart
│  ├─ main_qa.dart
│  └─ src
│     ├─ app_module.dart
│     ├─ app_widget.dart
│     ├─ i18n
│     │  └─ modules
│     │     └─ home_i18n.dart
│     ├─ modules
│     │  └─ home
│     │     ├─ home_controller.dart
│     │     ├─ home_controller.g.dart
│     │     ├─ home_module.dart
│     │     └─ home_page.dart
│     ├─ shared
│     │  ├─ clients
│     │  │  ├─ dio_client.dart
│     │  │  └─ hive_client.dart
│     │  ├─ constants
│     │  │  ├─ constants.dart
│     │  │  └─ features.dart
│     │  ├─ extensions
│     │  │  └─ extensions.dart
│     │  ├─ helpers
│     │  │  ├─ error_mapper.dart
│     │  │  └─ validators.dart
│     │  └─ widgets
│     │     ├─ custom_alert_dialog
│     │     │  ├─ custom_alert_dialog.dart
│     │     │  └─ types
│     │     │     ├─ confirm_dialog.dart
│     │     │     └─ error_dialog.dart
│     │     └─ observer_future
│     │        └─ observer_future.dart
│     ├─ styles
│     │  ├─ app_bar_theme_app.dart
│     │  ├─ app_color_scheme.dart
│     │  ├─ app_text_theme.dart
│     │  └─ app_theme_data.dart
│     └─ todo_repository.dart
└─ test
   └─ src
      ├─ i18n
      │  └─ modules
      │     └─ home_i18n_test.dart
      └─ modules
         └─ home
            ├─ home_controller_test.dart
            └─ home_page_test.dart

```

</details>
<details>
<summary>MVC with modular  <b>(complete mode)</b></summary>

```dart
example_mvc_modular_complete
├─ lib
│  ├─ app_flavor_values.dart
│  ├─ main_dev.dart
│  ├─ main_production.dart
│  ├─ main_qa.dart
│  └─ src
│     ├─ app_module.dart
│     ├─ app_widget.dart
│     ├─ i18n
│     │  ├─ login_i18n.dart
│     │  ├─ modules
│     │  │  └─ home_i18n.dart
│     │  └─ validators_i18n.dart
│     ├─ modules
│     │  ├─ home
│     │  │  ├─ home_controller.dart
│     │  │  ├─ home_controller.g.dart
│     │  │  ├─ home_module.dart
│     │  │  └─ home_page.dart
│     │  ├─ login
│     │  │  ├─ login_module.dart
│     │  │  └─ pages
│     │  │     ├─ login
│     │  │     │  ├─ login_controller.dart
│     │  │     │  ├─ login_controller.g.dart
│     │  │     │  └─ login_page.dart
│     │  │     └─ register
│     │  │        ├─ register_controller.dart
│     │  │        ├─ register_controller.g.dart
│     │  │        └─ register_page.dart
│     │  └─ splash
│     │     └─ splash_page.dart
│     ├─ shared
│     │  ├─ auth
│     │  │  ├─ auth_store.dart
│     │  │  ├─ auth_store.g.dart
│     │  │  └─ repositories
│     │  │     ├─ auth_repository.dart
│     │  │     └─ auth_repository_interface.dart
│     │  ├─ clients
│     │  │  ├─ dio_client.dart
│     │  │  └─ hive_client.dart
│     │  ├─ constants
│     │  │  ├─ constants.dart
│     │  │  └─ features.dart
│     │  ├─ extensions
│     │  │  └─ extensions.dart
│     │  ├─ helpers
│     │  │  ├─ error_mapper.dart
│     │  │  └─ validators.dart
│     │  ├─ models
│     │  │  └─ user_model.dart
│     │  └─ widgets
│     │     ├─ custom_alert_dialog
│     │     │  ├─ custom_alert_dialog.dart
│     │     │  └─ types
│     │     │     ├─ confirm_dialog.dart
│     │     │     └─ error_dialog.dart
│     │     ├─ email_text_field
│     │     │  └─ email_text_field_widget.dart
│     │     ├─ observer_future
│     │     │  └─ observer_future.dart
│     │     └─ password_text_field
│     │        └─ password_text_field_widget.dart
│     └─ styles
│        ├─ app_bar_theme_app.dart
│        ├─ app_color_scheme.dart
│        ├─ app_text_theme.dart
│        └─ app_theme_data.dart
└─ test
   └─ src
      ├─ i18n
      │  └─ modules
      │     └─ home_i18n_test.dart
      └─ modules
         └─ home
            ├─ home_controller_test.dart
            └─ home_page_test.dart

```

</details>

#### Options

The command allows to specify the architecture using the following options:

- Architecture:

```bash
-a <architecture_name>

Options:
clean / mvc_modular

Example:
snow start -a clean
```

This command asks for permission to erase lib folder. If you don't want to see the warning, type the -f (force) flag:

```bash
snow start -a clean -f
```

### Command `run`

Runs the scripts in pubspec.yaml:

```bash
snow run mobx
```

Example in pubspec.yaml:

```yaml
scripts:
  mobx: flutter pub run build_runner watch
```

You can also add vars to customize scripts

```yaml
vars:
  runner: flutter pub run build_runner
  clean: flutter clean
  get: flutter pub get

scripts:
    mobx_build: $clean & $get & $runner build --delete-conflicting-outputs
    mobx_watch: $clean & $get & $runner watch --delete-conflicting-outputs
```

Running:

```bash
snow run mobx_build
```

### Command `install`

Installs or update a package in dependencies:

```bash
snow install rxdart dio bloc_pattern
```

Result:

![Install example](https://github.com/Flutterando/slidy/blob/master/screenshots/dependencies.png?raw=true)

or you can just use the **i** command (both are the same)

```bash
snow i rxdart dio bloc_pattern
```

**Install packages as dev_dependency:**

```bash
snow i mockito --dev
```

Result:

![Folder example](https://github.com/Flutterando/slidy/blob/master/screenshots/dev_d.png?raw=true)

### Command `uninstall`

Removes a package from dependencies

```bash
snow uninstall dio
```

You can also remove a **dev_dependency** using the flag --dev

```bash
snow uninstall mockito --dev
```

### Command `update`

Updates a package from dependencies

```bash
snow update dio
```

You can also update a **dev_dependency** using the flag --dev

```bash
snow update mockito --dev
```

### Command `revert`

Reverts the last command.
E.g. When I created a page and by mistake I tipped wrong the name, then I run the revert command,
that will erases all the last files created(the page file and your test file).

```bash
snow revert
```

### Command `generate`

Creates a module, page, widget, controller, repository, entity, data source, use cases or tests.

> **NOTE:** You can replace `g` with `generate` command.

#### Creates a new module

This command generates a new module that will store the routes of the page and the binds(controllers, repositories or
any class that you want to inject).

> **NOTE:** You can create a "Complete Module" with Module, Page, Controller, tests for Page and for Controller using the
flag `-c`

<details>
<summary><b>E.g. HomeModule generated:</summary>

<a href="https://gitlab.com/snowman-labs/flutter-devs/snow_cli_flutter/-/tree/master/example_clean/lib/app/pages/home/home_module.dart">
Source File</a>

```dart
import 'package:example_clean/app/pages/home/home_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:example_clean/app/pages/home/home_page.dart';

class HomeModule extends Module {
  @override
  final List<Bind> binds = [
        Bind((i) => HomeController()),
      ];

  @override
  final List<ModularRoute> routes = [
        ChildRoute(Modular.initialRoute, child: (_, args) => HomePage()),
      ];
}
```

</details>

Commands to create:

```bash
snow g module <module_name>
```

or

```bash
snow g m <module_name>
```

#### Creates a new controller

This command generates a new controller with mobx state management.

<details>
<summary><b>E.g. HomeController generated:</summary>

<a href="https://gitlab.com/snowman-labs/flutter-devs/snow_cli_flutter/-/tree/master/example_clean/lib/app/pages/home/home_controller.dart">
Source File</a>

```dart
import 'package:mobx/mobx.dart';

part 'home_controller.g.dart';

class HomeController = _HomeControllerBase with _$HomeController;

abstract class _HomeControllerBase with Store {
  // @observable
  // int value = 0;
  //
  // @action
  // void increment() {
  //   value++;
  // }
}
```

</details>

Commands to create:

```bash
snow g controller <folder_name>/<page_name>
```

or

```bash
snow g c <folder_name>/<page_name>
```

#### Creates a new page

This command generates a new page with a controller.
> **NOTE:** If you want to create a page without controller use the flag `-c`

<details>
<summary><b>E.g. HomePage generated:</summary>

<a href="https://gitlab.com/snowman-labs/flutter-devs/snow_cli_flutter/-/tree/master/example_clean/lib/app/pages/home/home_page.dart">
Source File</a>

```dart
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'home_controller.dart';

class HomePage extends StatefulWidget {
  final String title;
  const HomePage({Key key, this.title = "Home"}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends ModularState<HomePage, HomeController> {
  //use 'controller' variable to access controller

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        children: <Widget>[],
      ),
    );
  }
}
```

</details>

Commands to create:

```bash
snow g page <folder_name>/<page_name>
```

or

```bash
snow g p <folder_name>/<page_name>
```

#### Creates a new widget

This command generates a new widget.
> **NOTE:** You can create a widget using a controller using the flag `-c`

<details>
<summary><b>E.g. HomeWidget generated:</summary>

Command on <a href="https://gitlab.com/snowman-labs/flutter-devs/snow_cli_flutter/-/tree/master/example_clean">example_clean</a>
project:

```bash
snow g widget widgets/example
```

<a href="https://gitlab.com/snowman-labs/flutter-devs/snow_cli_flutter/-/tree/master/example_clean/lib/app/widgets/example/example_widget.dart">
Source File</a>

```dart
import 'package:flutter/material.dart';

class ExampleWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text("textFinder"),
    );
  }
}
```

</details>

Commands to create:

```bash
snow g widget <folder_name>/<widget_name>
```

or

```bash
snow g w <folder_name>/<widget_name>
```

#### Creates a new Data Source (only Clean architecture)

This command generates two new data source files, being them, the local and the remote data source.

<details>
<summary><b>E.g. TodoLocalDataSource and TodoRemoteDataSource generated:</summary>

Command on <a href="https://gitlab.com/snowman-labs/flutter-devs/snow_cli_flutter/-/tree/master/example_clean">example_clean</a>
project:

```bash
snow g ds todo
```

TodoLocalDataSource:
<a href="https://gitlab.com/snowman-labs/flutter-devs/snow_cli_flutter/-/tree/master/example_clean/lib/data/data_sources/todo/todo_local_data_source.dart">
Source File</a>

```dart
import 'package:injectable/injectable.dart';
import 'package:example_clean/data/local/hive_client.dart';

@injectable
class TodoLocalDataSource {
  final HiveClient _hiveClient;
  static const String box = "Todo";

  const TodoLocalDataSource(this._hiveClient);
}
```

TodoRemoteDataSource:
<a href="https://gitlab.com/snowman-labs/flutter-devs/snow_cli_flutter/-/tree/master/example_clean/lib/data/data_sources/todo/todo_remote_data_source.dart">
Source File</a>

```dart
import 'package:injectable/injectable.dart';

@injectable
class TodoRemoteDataSource {
  const TodoRemoteDataSource();
}
```

</details>

Commands to create:

```bash
snow g data_source <data_source_name>
```

or

```bash
snow g ds <data_source_name>
```

Choose what kind of Data Source do you want to create:

```bash
WARN: What kind of data source do you want to generate?
Local and Remote (default)
Only Local
Only Remote
```

#### Creates a new Entity (only Clean architecture)

This command generates a basic entity, with your mapper extension and updates the file `mappers/index.dart` to better imports
and better uses.

<details>
<summary><b>E.g. TodoEntity and TodoMapper generated:</summary>

Command on <a href="https://gitlab.com/snowman-labs/flutter-devs/snow_cli_flutter/-/tree/master/example_clean">example_clean</a>
project:

```bash
snow g e todo
```

TodoEntity:
<a href="https://gitlab.com/snowman-labs/flutter-devs/snow_cli_flutter/-/tree/master/example_clean/lib/domain/entities/todo/todo_entity.dart">
Source File</a>

```dart
import 'package:flutter/foundation.dart';

@immutable
class TodoEntity {
  const TodoEntity();

  @override
  String toString() => 'TodoInfo()'; // TODO Write toString of Todo

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is TodoEntity; // TODO Write == operator of Todo
  }

  //@override
  //int get hashCode => property.hashCode; // TODO hashCode overrides of Todo

}
```

TodoMapper:
<a href="https://gitlab.com/snowman-labs/flutter-devs/snow_cli_flutter/-/tree/master/example_clean/lib/data/mappers/todo/todo_mapper.dart">
Source File</a>

```dart
import 'dart:convert';

import 'package:example_clean/domain/entities/todo/todo_entity.dart';

extension TodoMapper on TodoEntity {
  TodoEntity copyWith(
      // TODO Write copyWith of Todo
      ) {
    return TodoEntity(
        // TODO Write copyWith of Todo
        );
  }

  Map<String, dynamic> toMap() {
    return {
      // TODO Write toMap of Todo
    };
  }

  TodoEntity fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return TodoEntity(
        // TODO Write fromMap of Todo
        );
  }

  String toJson() => json.encode(toMap());

  TodoEntity fromJson(String source) => fromMap(json.decode(source));
}
```

</details>

Commands to create:

```bash
snow g entity <entity_name>
```

or

```bash
snow g e <entity_name>
```

#### Creates a new Use case (only Clean architecture)

This command generates a use case.

<details>
<summary><b>E.g. GetTodoListUseCase generated:</summary>

Command on <a href="https://gitlab.com/snowman-labs/flutter-devs/snow_cli_flutter/-/tree/master/example_clean">example_clean</a>
project:

```bash
snow g u get_todo_list
```

<a href="https://gitlab.com/snowman-labs/flutter-devs/snow_cli_flutter/-/tree/master/example_clean/lib/domain/usecases/get_todo_list_use_case.dart">
Source File</a>

```dart
import 'package:example_clean/domain/usecases/base/base_future_use_case.dart';
import 'package:injectable/injectable.dart';

@injectable
class GetTodoListUseCase implements BaseFutureUseCase<void, void> {
  const GetTodoListUseCase();

  @override
  Future<void> call([void params]) {
    // TODO: implement call
    throw UnimplementedError();
  }
}
```

</details>

Commands to create:

```bash
snow g use_case <use_case_name>
```

or

```bash
snow g u <use_case_name>
```

#### Create a new repository

This command creates a new repository that will generate according to the architecture of the project.
> **NOTE:** If you want to create a complete repository, with the local and the remote data source use the flag `-c`
> (only for clean architecture)

<details>
<summary><b>E.g. TodoRepository generated: (Clean architecture)</b></summary>

Command on <a href="https://gitlab.com/snowman-labs/flutter-devs/snow_cli_flutter/-/tree/master/example_clean">example_clean</a>
project:

```bash
snow g r todo
```

TodoRepository:
<a href="https://gitlab.com/snowman-labs/flutter-devs/snow_cli_flutter/-/tree/master/example_clean/lib/domain/repositories/todo/todo_repository.dart">
Source File</a>

```dart
abstract class TodoRepository {
  // TODO Write methods of TodoRepository
}
```

TodoRepositoryImpl:
<a href="https://gitlab.com/snowman-labs/flutter-devs/snow_cli_flutter/-/tree/master/example_clean/lib/data/repositories/todo/todo_repository_impl.dart">
Source File</a>

```dart
import 'package:injectable/injectable.dart';
import 'package:example_clean/domain/repositories/todo/todo_repository.dart';

@Injectable(as: TodoRepository)
class TodoRepositoryImpl implements TodoRepository {
  const TodoRepositoryImpl();
}
```

</details>

<details>
<summary><b>E.g. TodoRepository generated: (Clean architecture, mode complete "-c")</b></summary>

Command on <a href="https://gitlab.com/snowman-labs/flutter-devs/snow_cli_flutter/-/tree/master/example_clean">example_clean</a>
project:

```bash
snow g r todo -c
```

TodoLocalDataSource:
<a href="https://gitlab.com/snowman-labs/flutter-devs/snow_cli_flutter/-/tree/master/example_clean/lib/data/data_sources/todo/todo_local_data_source.dart">
Source File</a>

```dart
import 'package:injectable/injectable.dart';
import 'package:example_clean/data/local/hive_client.dart';

@injectable
class TodoLocalDataSource {
  final HiveClient _hiveClient;
  static const String box = "Todo";

  const TodoLocalDataSource(this._hiveClient);
}
```

TodoRemoteDataSource:
<a href="https://gitlab.com/snowman-labs/flutter-devs/snow_cli_flutter/-/tree/master/example_clean/lib/data/data_sources/todo/todo_remote_data_source.dart">
Source File</a>

```dart
import 'package:injectable/injectable.dart';

@injectable
class TodoRemoteDataSource {
  const TodoRemoteDataSource();
}
```

TodoRepository:
<a href="https://gitlab.com/snowman-labs/flutter-devs/snow_cli_flutter/-/tree/master/example_clean/lib/domain/repositories/todo/todo_repository.dart">
Source File</a>

```dart
abstract class TodoRepository {
  // TODO Write methods of TodoRepository
}
```

TodoRepositoryImpl:
<a href="https://gitlab.com/snowman-labs/flutter-devs/snow_cli_flutter/-/tree/master/example_clean/lib/data/repositories/todo/todo_repository_impl.dart">
Source File</a>

```dart
import 'package:injectable/injectable.dart';
import 'package:example_clean/domain/repositories/todo/todo_repository.dart';

@Injectable(as: TodoRepository)
class TodoRepositoryImpl implements TodoRepository {
  const TodoRepositoryImpl();
}
```

</details>

<details>
<summary><b>E.g. TodoRepository generated: (MVC modular)</b></summary>

Command on <a href="https://gitlab.com/snowman-labs/flutter-devs/snow_cli_flutter/-/tree/master/example_mvc_modular">example_mvc_modular</a>
project:

```bash
snow g r todo
```

TodoRepository:
<a href="https://gitlab.com/snowman-labs/flutter-devs/snow_cli_flutter/-/tree/master/example_mvc_modular/lib/src/modules/home/repositories/todo_repository.dart">
Source File</a>

```dart
import 'package:flutter_modular/flutter_modular.dart';
import 'package:dio/dio.dart';

class TodoRepository extends Disposable {
  Future fetchPost(Dio client) async {
    final response =
        await client.get('https://jsonplaceholder.typicode.com/posts/1');
    return response.data;
  }

  //dispose will be called automatically
  @override
  void dispose() {}
}
```

</details>

Commands to create:

```bash
snow g repository <repository_name>
```

or

```bash
snow g r <repository_name>
```

#### Generating Unit tests

Generate **unit tests** on the test folder for you.

```bash
snow test <folder_name>/<dart_file>.dart
```

## ✅ Run tests

```bash
pub run test
```

## 👤 Author

### Snowman labs

<img alt="snowmanlabs-logo" align="right"
  src="https://www.snowmanlabs.com.br/wp-content/uploads/logo_snowmanlabs_fundo_claro.png" height="100"/>

- Website: [snowmanlabs.com](https://snowmanlabs.com)
- Twitter: [@SnowmanLabs](https://twitter.com/SnowmanLabs)
- Github: [@snowmanlabs](https://github.com/snowmanlabs)
- LinkedIn: [@SnowmanLabs](https://linkedin.com/company/snowman-labs)

## 🤝 Contributing

Contributions, issues and feature requests are welcome!<br />
Feel free to check [issues page](https://gitlab.com/snowman-labs/flutter-devs/snow_cli_flutter/-/issues).
You can also take a look at the [contributing guide](https://gitlab.com/snowman-labs/flutter-devs/snow_cli_flutter/-/blob/master/CHANGELOG.md).

## 💚 Show your support

<img align="right" width="20%" src="https://gitlab.com/snowman-labs/flutter-devs/snow_cli_flutter/-/raw/master/images/snow_blower_logo.png">

Give a ⭐️ if this project helped you!

## 📝 License

Copyright © 2020 [Snowman labs](https://github.com/snowmanlabs).<br />
This project is [BSD-3 Clause](https://opensource.org/licenses/BSD-3-Clause) licensed.

[clean]: https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html
