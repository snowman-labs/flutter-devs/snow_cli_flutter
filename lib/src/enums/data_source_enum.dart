enum DataSource{
  localAndRemote, onlyRemote, onlyLocal
}

extension DataSourceMapper on DataSource {
  static DataSource fromString(String dataSource) {
    switch (dataSource) {
      case 'localAndRemote':
        return DataSource.localAndRemote;
        break;
      case 'onlyLocal':
        return DataSource.onlyLocal;
        break;
      case 'onlyRemote':
        return DataSource.onlyRemote;
        break;
      default:
        return null;
    }
  }
}