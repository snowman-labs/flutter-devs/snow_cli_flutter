enum Persistence { hive, moor, hiveAndMoor }

const defaultPersistence = Persistence.hive;

extension PersistenceMapper on Persistence {
  static Persistence fromString(String architecture) {
    switch (architecture) {
      case 'hive':
        return Persistence.hive;
        break;
      case 'moor':
        return Persistence.moor;
        break;
      case 'hive_moor':
        return Persistence.hiveAndMoor;
        break;
      default:
        return null;
    }
  }
}
