import 'package:flutter_snow_blower/src/utils/object_generate.dart';

String moduleGeneratorModular(ObjectGenerate obj) {
  var path = obj.pathModule.replaceFirst('lib/', '');
  var pkg = obj.packageName;

  var import = pkg.isNotEmpty ? "import 'package:$pkg/${path}_page.dart';" : '';
  var router = pkg.isNotEmpty
      ? "ChildRoute(Modular.initialRoute, child: (_, args) => ${obj.name}Page()),"
      : '';

  return '''
  import 'package:flutter_modular/flutter_modular.dart';
  ${import.replaceFirst('$pkg/$pkg', pkg)}
  class ${obj.name}Module extends Module {
  @override
  final List<Bind> binds = [];

  @override
  final List<ModularRoute> routes = [
    $router
  ];

}
  ''';
}

String moduleGeneratorModularNoRoute(ObjectGenerate obj) {
  var path = obj.pathModule.replaceFirst('lib/', '');
  var pkg = obj.packageName;
  var import = pkg.isNotEmpty ? "import 'package:$pkg/${path}_page.dart';" : '';

  return '''
  import 'package:flutter_modular/flutter_modular.dart';
  import 'package:flutter/material.dart';
  ${import.replaceFirst('$pkg/$pkg', pkg)}
  
  // ignore: must_be_immutable
  class ${obj.name}Module extends WidgetModule {
  @override
  final List<Bind> binds = [];

  @override  
  Widget get view => ${obj.name}Page();
}
  ''';
}
