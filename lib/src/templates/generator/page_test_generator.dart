import 'package:flutter_snow_blower/src/utils/object_generate.dart';

String pageTestGenerator(ObjectGenerate obj) {
  var package = "import 'package:flutter_modular_test/flutter_modular_test.dart'";

  var importPage =
      'package:${obj.packageName}/${obj.import.replaceFirst("lib/", "").replaceAll("\\", "/")}'
          .replaceFirst('${obj.packageName}/${obj.packageName}', obj.packageName);

  return '''
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

main() {
  testWidgets('${obj.name}Page has title', (WidgetTester tester) async {
  //  await tester.pumpWidget(buildTestableWidget(${obj.name}Page(title: '${obj.name}')));
  //  final titleFinder = find.text('${obj.name}');
  //  expect(titleFinder, findsOneWidget);
  });
}
  ''';
}
