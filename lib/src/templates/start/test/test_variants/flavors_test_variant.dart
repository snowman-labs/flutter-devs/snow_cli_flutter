import 'package:flutter_snow_blower/src/enums/architecture_enum.dart';

String flavorsTestVariant(String package, Architecture architecture) {
  String imports;

  switch (architecture) {
    case Architecture.cleanArchitecture:
      imports = '''
import 'package:$package/data/constants/constants.dart';
import 'package:$package/di/di.dart';
''';
      break;
    case Architecture.mvcModular:
      imports = '''
import 'package:$package/src/shared/constants/constants.dart';
''';
      break;
  }

  return '''
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_snow_base/flutter_snow_base.dart';
$imports

class FlavorsTestVariant implements TestVariant<FlavorConfig Function()> {
  @override
  String describeValue(FlavorConfig Function() value) {
    value();
    return FlavorConfig.name;
  }

  @override
  Future<FlavorConfig Function()> setUp(FlavorConfig Function() value) async{
    value();
    ${architecture == Architecture.cleanArchitecture ? "configureInjection();" : ""}
    return null;
  }

  @override
  Iterable<FlavorConfig Function()> values = [
    () => FlavorConfig.tests(
      flavor: Flavor.dev,
      values: Constants.flavorDev,
    ),
    () => FlavorConfig.tests(
      flavor: Flavor.qa,
      values: Constants.flavorQa,
    ),
    () => FlavorConfig.tests(
      flavor: Flavor.production,
      values: Constants.flavorProd,
    ),
  ];

  @override
  Future<void> tearDown(FlavorConfig Function() value, FlavorConfig Function() memento) async{
    
  }
}
''';
}
