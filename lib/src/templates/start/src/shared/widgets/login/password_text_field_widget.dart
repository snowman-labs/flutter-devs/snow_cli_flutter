import 'package:flutter_snow_blower/src/enums/architecture_enum.dart';

String passwordTextFieldWidget(
    String package, String srcOrApp, Architecture architecture) {
  String imports;

  switch (architecture) {
    case Architecture.cleanArchitecture:
      imports =
          '''import 'package:$package/$srcOrApp/utils/validators.dart';''';
      break;
    case Architecture.mvcModular:
      imports =
          '''import 'package:$package/$srcOrApp/shared/helpers/validators.dart';''';
      break;
  }

  return '''
import 'package:flutter/material.dart';
$imports

class PasswordTextFieldWidget extends StatefulWidget {
  final TextEditingController controller;
  final void Function(String) onSaved;
  final String label;

  PasswordTextFieldWidget(
      {Key key, this.controller, this.onSaved, this.label,})
      : super(key: key);

  @override
  _PasswordTextFieldWidgetState createState() =>
      _PasswordTextFieldWidgetState();
}

class _PasswordTextFieldWidgetState extends State<PasswordTextFieldWidget> {
  bool obscure = true;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      obscureText: obscure,
      controller: widget.controller,
      onSaved: widget.onSaved,
      validator: Validators.password,
      decoration: InputDecoration(
        labelText: widget.label,
        suffixIcon: IconButton(
          icon: Icon(obscure ? Icons.visibility_off : Icons.visibility),
          onPressed: () {
            setState(() {
              obscure = !obscure;
            });
          },
        ),
      ),
    );
  }
}
''';
}
