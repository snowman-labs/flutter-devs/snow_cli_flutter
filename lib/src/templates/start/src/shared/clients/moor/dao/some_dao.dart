String someDao(String package, bool injectable) => '''
import 'package:injectable/injectable.dart';
import 'package:moor/moor.dart';
import 'package:$package/data/local/moor_db.dart';
import 'package:$package/data/model/local/some_table.dart';

part 'some_dao.g.dart';

${injectable ? '@injectable' : ''}
@UseDao(tables: [SomeTable])
class SomeDao extends DatabaseAccessor<MoorDB> with _\$SomeDaoMixin {
  SomeDao(MoorDB db) : super(db);


  Future<List<SomeTableData>> getAll(){
   return select(someTable).get();
  }
  
  Future<SomeTableData> getSomeByUid(String uid){
   return (select(someTable)..where((tbl) => tbl.uid.equals(uid))).getSingle();
  }

  Future<int> insertSome(SomeTableData entry) {
    return into(someTable).insert(entry, mode: InsertMode.insertOrReplace);
  }

  Future<bool> updateSome(SomeTableData entry) {
    return (update(someTable).replace(entry)).then((value) {
      print(value ? "Update goal row success" : "Update goal row failed");
      return value;
    });
  }

}
''';
