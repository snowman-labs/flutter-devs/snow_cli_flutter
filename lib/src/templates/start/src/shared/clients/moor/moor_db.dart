String moorDb(String package, bool useInjectable) => '''
import 'dart:io';
${useInjectable ? "import 'package:injectable/injectable.dart';" : ""}
import 'package:moor/moor.dart';
import 'package:moor_ffi/moor_ffi.dart';
import 'package:path/path.dart' as p;
import 'package:path_provider/path_provider.dart';
import 'package:flutter_snow_base/flutter_snow_base.dart';
import 'package:$package/data/local/dao/some_dao.dart';
import 'package:$package/data/model/local/some_table.dart';

part 'moor_db.g.dart';

/// Visit the Moor Documentation at https://moor.simonbinder.eu/docs/other-engines/vm/#migrating-from-moor-flutter-to-moor-ffi

LazyDatabase _init() {
  return LazyDatabase(() async {
    final dbFolder = await getApplicationDocumentsDirectory();
    final file = File(p.join(dbFolder.path,
        "my_db_\${FlavorConfig.name.toLowerCase()}.sqlite"));
    return VmDatabase(file);
  });
}

${useInjectable ? "@singleton" : ""}
@UseMoor(tables: [SomeTable], daos: [SomeDao])
class MoorDB extends _\$MoorDB {
  MoorDB() : super(_init());

  @override
  int get schemaVersion => 1;

  @override
  MigrationStrategy get migration => MigrationStrategy(
      onCreate: (m) {
        return m.createAll();
      },
      onUpgrade: (m, from, to) async {});

  Future<void> resetDb() async {
    for (var table in allTables) {
      await delete(table).go();
    }
  }
}
''';
