String splashPageClean() => '''
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'splash_controller.dart';
import 'package:i18n_extension/i18n_widget.dart';

class SplashPage extends StatefulWidget {
  final String title;
  const SplashPage({Key key, this.title = "Splash"}) : super(key: key);

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  Widget build(BuildContext context) {
    // Instancia o SplashController após ter um contexto,
    // assim consegue fazer navigation para o login
    Modular.get<SplashController>();

    return I18n(
      child: Scaffold(
        body: Center(
          child: CircularProgressIndicator(),
        ),
      ),
    );
  }
}
''';

String splashPageCleanWithFlutterIntl() => '''
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'splash_controller.dart';

class SplashPage extends StatefulWidget {
  final String title;
  const SplashPage({Key key, this.title = "Splash"}) : super(key: key);

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  Widget build(BuildContext context) {
    // Instancia o SplashController após ter um contexto,
    // assim consegue fazer navigation para o login
    Modular.get<SplashController>();

    return Scaffold(
        body: Center(
          child: CircularProgressIndicator(),
        ),
      );
  }
}
''';

String splashPageModular(String package) => '''
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:i18n_extension/i18n_widget.dart';
import 'package:$package/src/shared/auth/auth_store.dart';

class SplashPage extends StatefulWidget {
  
  final String title;
  const SplashPage({Key key, this.title = "Splash"}) : super(key: key);

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  Widget build(BuildContext context) {
    Modular.get<AuthStore>();
    return I18n(
      child: Scaffold(
        body: Center(
          child: CircularProgressIndicator(),
        ),
      ),
    );
  }
}
  ''';

String splashPageModularWithFlutterIntl(String package) => '''
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:$package/src/shared/auth/auth_store.dart';

class SplashPage extends StatefulWidget {
  
  final String title;
  const SplashPage({Key key, this.title = "Splash"}) : super(key: key);

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  Widget build(BuildContext context) {
    Modular.get<AuthStore>();
    return Scaffold(
        body: Center(
          child: CircularProgressIndicator(),
        ),
      );
  }
}
  ''';
