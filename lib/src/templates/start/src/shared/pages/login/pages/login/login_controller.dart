String loginControllerClean(String package) => '''
import 'package:mobx/mobx.dart';
import 'package:$package/di/di.dart';
import 'package:$package/domain/entities/auth_entity.dart';
import 'package:$package/domain/entities/user_entity.dart';
import 'package:$package/domain/usecases/auth/login_user_email_use_case.dart';

part 'login_controller.g.dart';

class LoginController = _LoginBase with _\$LoginController;

abstract class _LoginBase with Store {
  final loginUserEmail = getIt.get<LoginUserEmailUseCase>();

  @observable
  bool loading = false;
  
  @action
  Future<bool> loginWithEmail(String email, String password) async {
    UserEntity user;
    try {
      loading = true;
      user = await loginUserEmail(AuthEntity(email: email, password: password));
    } catch (e) {} finally {
      loading = false;
    }
    return user != null;
  }
}
''';

String loginControllerModular(String package) => '''
import 'package:$package/src/shared/auth/auth_store.dart';
import 'package:$package/src/shared/models/user_model.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';

part 'login_controller.g.dart';

class LoginController = _LoginBase with _\$LoginController;

abstract class _LoginBase with Store {
  final authStore = Modular.get<AuthStore>();

  @observable
  bool loading = false;

  @action
  Future<bool> loginWithEmail(String email, String password) async {
    UserModel user;
    try {
      loading = true;
      user = await authStore.loginWithEmail(email, password);
    } catch (e) {} finally {
      loading = false;
    }
    return user != null;
  }
}

''';
