String loginModule() => '''
import 'package:flutter_modular/flutter_modular.dart';

import 'pages/login/login_controller.dart';
import 'pages/login/login_page.dart';
import 'pages/register/register_controller.dart';
import 'pages/register/register_page.dart';

class LoginModule extends Module {
  @override
  final List<Bind> binds = [
        Bind((i) => RegisterController()),
        Bind((i) => LoginController()),
      ];

  @override
  final List<ModularRoute> routes = [
        ChildRoute('/', child: (_, args) => LoginPage()),
        ChildRoute('/register', child: (_, args) => RegisterPage()),
      ];
}
''';
