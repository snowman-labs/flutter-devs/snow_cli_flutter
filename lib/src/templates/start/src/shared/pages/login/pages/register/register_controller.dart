String registerControllerClean(String package) => '''
import 'package:mobx/mobx.dart';
import 'package:$package/di/di.dart';
import 'package:$package/domain/entities/auth_entity.dart';
import 'package:$package/domain/entities/user_entity.dart';
import 'package:$package/domain/usecases/auth/register_user_email_use_case.dart';

part 'register_controller.g.dart';

class RegisterController = _RegisterControllerBase with _\$RegisterController;

abstract class _RegisterControllerBase with Store {
  final registerUserEmail = getIt.get<RegisterUserEmailUseCase>();

  @observable
  bool loading = false;

  @action
  Future<bool> registerWithEmail(String email, String password) async {
    UserEntity user;
    try {
      loading = true;
      user = await registerUserEmail(AuthEntity(email: email, password: password));
    } finally {
      loading = false;
    }
    return user != null;
  }
}
''';

String registerControllerModular(String package) => '''
import 'package:$package/src/shared/auth/auth_store.dart';
import 'package:$package/src/shared/models/user_model.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';

part 'register_controller.g.dart';

class RegisterController = _RegisterControllerBase with _\$RegisterController;

abstract class _RegisterControllerBase with Store {
  final authStore = Modular.get<AuthStore>();

  @observable
  bool loading = false;

  @action
  Future<bool> registerWithEmail(String email, String password) async {
    UserModel user;
    try {
      loading = true;
      user =
          await authStore.registerWithEmail(email, password);
    } finally {
      loading = false;
    }
    return user != null;
  }
}

''';
