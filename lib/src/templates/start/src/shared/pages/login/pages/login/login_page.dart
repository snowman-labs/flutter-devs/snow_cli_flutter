import 'package:flutter_snow_blower/src/enums/architecture_enum.dart';

String loginPage(String package, Architecture architecture) {
  String imports;

  switch (architecture) {
    case Architecture.cleanArchitecture:
      imports = '''
import 'package:$package/app/i18n/login_i18n.dart';
import 'package:$package/app/widgets/custom_alert_dialog/custom_alert_dialog.dart';
import 'package:$package/app/widgets/login/email_text_field_widget.dart';
import 'package:$package/app/widgets/login/password_text_field_widget.dart';
''';
      break;
    case Architecture.mvcModular:
      imports = '''
import 'package:$package/src/i18n/login_i18n.dart';
import 'package:$package/src/shared/widgets/custom_alert_dialog/custom_alert_dialog.dart';
import 'package:$package/src/shared/widgets/email_text_field/email_text_field_widget.dart';
import 'package:$package/src/shared/widgets/password_text_field/password_text_field_widget.dart';
''';
      break;
  }

  return '''
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
$imports

import 'login_controller.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends ModularState<LoginPage, LoginController> {
  final _formKey = GlobalKey<FormState>();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();

  Future<void> login() async {
    if (_formKey.currentState.validate()) {
      if (!await controller.loginWithEmail(
        _emailController.text,
        _passwordController.text,
      )) {
        CustomAlertDialog.error(context, "Usuario inexistente ou senha incorreta");
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Entrar".i18n),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 16),
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              EmailTextFieldWidget(
                controller: _emailController,
                label: "E-mail".i18n,
              ),
              PasswordTextFieldWidget(
                controller: _passwordController,
                label: "Senha".i18n,
              ),
              SizedBox(
                height: 25,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  RaisedButton(
                    onPressed: () => Modular.to.pushNamed("/login/register"),
                    child: Text("Cadastrar-se".i18n),
                  ),
                  Observer(builder: (_) {
                    return RaisedButton(
                      onPressed: controller.loading ? null : login,
                      child: Text("Entrar".i18n),
                    );
                  }),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
''';
}

String loginPageWithFlutterIntl(String package, Architecture architecture) {
  String imports;

  switch (architecture) {
    case Architecture.cleanArchitecture:
      imports = '''
import 'package:$package/app/widgets/custom_alert_dialog/custom_alert_dialog.dart';
import 'package:$package/app/widgets/login/email_text_field_widget.dart';
import 'package:$package/app/widgets/login/password_text_field_widget.dart';
''';
      break;
    case Architecture.mvcModular:
      imports = '''
import 'package:$package/src/shared/widgets/custom_alert_dialog/custom_alert_dialog.dart';
import 'package:$package/src/shared/widgets/email_text_field/email_text_field_widget.dart';
import 'package:$package/src/shared/widgets/password_text_field/password_text_field_widget.dart';
''';
      break;
  }

  return '''
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
$imports

import 'login_controller.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends ModularState<LoginPage, LoginController> {
  final _formKey = GlobalKey<FormState>();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();

  Future<void> login() async {
    if (_formKey.currentState.validate()) {
      if (!await controller.loginWithEmail(
        _emailController.text,
        _passwordController.text,
      )) {
        CustomAlertDialog.error(context, "Usuario inexistente ou senha incorreta");
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Entrar"),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 16),
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              EmailTextFieldWidget(
                controller: _emailController,
                label: "E-mail",
              ),
              PasswordTextFieldWidget(
                controller: _passwordController,
                label: "Senha",
              ),
              SizedBox(
                height: 25,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  RaisedButton(
                    onPressed: () => Modular.to.pushNamed("/login/register"),
                    child: Text("Cadastrar-se"),
                  ),
                  Observer(builder: (_) {
                    return RaisedButton(
                      onPressed: controller.loading ? null : login,
                      child: Text("Entrar"),
                    );
                  }),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
''';
}
