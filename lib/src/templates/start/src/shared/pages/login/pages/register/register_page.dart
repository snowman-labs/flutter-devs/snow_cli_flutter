import 'package:flutter_snow_blower/src/enums/architecture_enum.dart';

String registerPage(String package, Architecture architecture) {
  String imports;

  switch (architecture) {
    case Architecture.cleanArchitecture:
      imports = '''
import 'package:$package/app/i18n/login_i18n.dart';
import 'package:$package/app/widgets/custom_alert_dialog/custom_alert_dialog.dart';
import 'package:$package/app/widgets/login/email_text_field_widget.dart';
import 'package:$package/app/widgets/login/password_text_field_widget.dart';
''';
      break;
    case Architecture.mvcModular:
      imports = '''
import 'package:$package/src/i18n/login_i18n.dart';
import 'package:$package/src/shared/widgets/custom_alert_dialog/custom_alert_dialog.dart';
import 'package:$package/src/shared/widgets/email_text_field/email_text_field_widget.dart';
import 'package:$package/src/shared/widgets/password_text_field/password_text_field_widget.dart';
''';
      break;
  }

  return '''
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
$imports

import 'register_controller.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key key}) : super(key: key);

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState
    extends ModularState<RegisterPage, RegisterController> {
  //use 'controller' variable to access controller
  final _formKey = GlobalKey<FormState>();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();

  Future<void> register() async {
    if (_formKey.currentState.validate()) {
      if (!await controller.registerWithEmail(
        _emailController.text,
        _passwordController.text,
      )) {
        CustomAlertDialog.error(context, "Erro no cadastro");
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Cadastro".i18n),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 16),
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              EmailTextFieldWidget(
                controller: _emailController,
                label: "E-mail".i18n,
              ),
              PasswordTextFieldWidget(
                controller: _passwordController,
                label: "Senha".i18n,
              ),
              SizedBox(
                height: 25,
              ),
              Observer(builder: (_) {
                return RaisedButton(
                  onPressed: controller.loading ? null : register,
                  child: Text("Cadastrar-se".i18n),
                );
              }),
            ],
          ),
        ),
      ),
    );
  }
}
''';
}

String registerPageWithFlutterIntl(String package, Architecture architecture) {
  String imports;

  switch (architecture) {
    case Architecture.cleanArchitecture:
      imports = '''
import 'package:$package/app/widgets/custom_alert_dialog/custom_alert_dialog.dart';
import 'package:$package/app/widgets/login/email_text_field_widget.dart';
import 'package:$package/app/widgets/login/password_text_field_widget.dart';
''';
      break;
    case Architecture.mvcModular:
      imports = '''
import 'package:$package/src/shared/widgets/custom_alert_dialog/custom_alert_dialog.dart';
import 'package:$package/src/shared/widgets/email_text_field/email_text_field_widget.dart';
import 'package:$package/src/shared/widgets/password_text_field/password_text_field_widget.dart';
''';
      break;
  }

  return '''
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
$imports

import 'register_controller.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key key}) : super(key: key);

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState
    extends ModularState<RegisterPage, RegisterController> {
  //use 'controller' variable to access controller
  final _formKey = GlobalKey<FormState>();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();

  Future<void> register() async {
    if (_formKey.currentState.validate()) {
      if (!await controller.registerWithEmail(
        _emailController.text,
        _passwordController.text,
      )) {
        CustomAlertDialog.error(context, "Erro no cadastro");
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Cadastro"),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 16),
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              EmailTextFieldWidget(
                controller: _emailController,
                label: "E-mail",
              ),
              PasswordTextFieldWidget(
                controller: _passwordController,
                label: "Senha",
              ),
              SizedBox(
                height: 25,
              ),
              Observer(builder: (_) {
                return RaisedButton(
                  onPressed: controller.loading ? null : register,
                  child: Text("Cadastrar-se"),
                );
              }),
            ],
          ),
        ),
      ),
    );
  }
}
''';
}
