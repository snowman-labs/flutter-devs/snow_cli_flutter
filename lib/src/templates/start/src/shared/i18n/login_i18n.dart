String loginI18n() => '''
import 'package:i18n_extension/i18n_extension.dart';

// For more info, see: https://pub.dartlang.org/packages/i18n_extension

extension LoginLocalization on String {
  static var t = Translations("pt_br") + 
      {
        "pt_br": "Cadastro",
        "en_us": "Register",
      } +
      {
        "pt_br": "Entrar",
        "en_us": "Login",
      } +
      {
        "pt_br": "E-mail",
        "en_us": "E-mail",
      } +
      {
        "pt_br": "Senha",
        "en_us": "Password",
      } +
      {
        "pt_br": "Cadastrar-se",
        "en_us": "Register",
      };

  String get i18n => localize(this, t);

  String fill(List<Object> params) => localizeFill(this, params);

  String plural(int value) => localizePlural(value, this, t);

  String version(Object modifier) => localizeVersion(modifier, this, t);

  Map<String, String> allVersions() => localizeAllVersions(this, t);
}
''';
