String authRemoteDataSource() => '''
import 'package:firebase_auth/firebase_auth.dart';
import 'package:injectable/injectable.dart';
import 'package:flutter/services.dart';

@injectable
class AuthRemoteDataSource {
  final FirebaseAuth _firebaseAuth;

  AuthRemoteDataSource(this._firebaseAuth);

  User getFirebaseUser() {
    return _firebaseAuth.currentUser;
  }

  Future<String> loginUserEmail(String email, String password,
      {bool saveAuthInfo = true}) async {
    try {
      final result = await _firebaseAuth.signInWithEmailAndPassword(
          email: email, password: password);
      return result.user.email;
    } on PlatformException catch (e) {
      if (e.code == "ERROR_NETWORK_REQUEST_FAILED") {
        return _firebaseAuth.currentUser.email;
      }
    }
    return null;
  }

  Future<void> logoutUser() {
    return _firebaseAuth.signOut();
  }

  Future<String> registerUserEmail(String email, String password) async {
    final result = await _firebaseAuth.createUserWithEmailAndPassword(
        email: email, password: password);
    return result.user.email;
  }
}
''';
