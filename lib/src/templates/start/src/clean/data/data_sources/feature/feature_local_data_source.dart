import 'package:flutter_snow_blower/src/enums/persistence_enum.dart';

String featureLocalDataSource(String package, Persistence persistence) => '''
import 'package:injectable/injectable.dart';
${persistence == Persistence.hive ? "import 'package:$package/data/local/hive_client.dart';" : persistence == Persistence.moor ? "import 'package:$package/data/local/dao/some_dao.dart';" : persistence == Persistence.hiveAndMoor ? "import 'package:$package/data/local/dao/some_dao.dart';"
        "import 'package:$package/data/local/hive_client.dart';" : ""}
import 'package:$package/data/mappers/index.dart';

/// Your [LocalDataSource] encapsulate your storage data source,
/// whether [SharedPreferences], [Hive] or [Moor].
/// This makes it easy to change where your data comes from.
/// Notice that you can also work with more than one storage client.

@injectable
class FeatureLocalDataSource {

  ${persistence == Persistence.hive ? 'final HiveClient _hiveClient;' : persistence == Persistence.moor ? 'final SomeDao _someDao;' : persistence == Persistence.hiveAndMoor ? 'final HiveClient _hiveClient;'
        'final SomeDao _someDao;' : ""}

  FeatureLocalDataSource(${persistence == Persistence.hive ? 'this._hiveClient' : persistence == Persistence.moor ? 'this._someDao' : persistence == Persistence.hiveAndMoor ? 'this._hiveClient, this._someDao' : ''});

  Future getSomethingFromMyStorage() async {
    // TODO: implement call
    throw UnimplementedError();
  }

}
''';
