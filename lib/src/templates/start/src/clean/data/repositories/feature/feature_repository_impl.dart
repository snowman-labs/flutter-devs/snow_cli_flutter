String featureRepoImpl(String package, {bool hasPersistence = true}) => '''
import 'dart:async';

import 'package:injectable/injectable.dart';
${ hasPersistence ?
"import 'package:$package/data/data_sources/feature/feature_local_data_source.dart';" : ""}
import 'package:$package/data/data_sources/feature/feature_remote_data_source.dart';
import 'package:$package/domain/repositories/feature/feature_repository.dart';

import 'package:$package/data/mappers/index.dart';

/// Every [RepositoryImpl] at Data Layer implements your corresponding [Repository] from Domain layer
/// Retrieve data from databases or other methods through [_localDataSource].
/// Responsible for any API calls through [_remoteDataSource].

@Singleton(as: FeatureRepository)
class FeatureRepositoryImpl implements FeatureRepository {

  final FeatureRemoteDataSource _remoteDataSource;
  ${hasPersistence ? "final FeatureLocalDataSource _localDataSource;" : ""}

  FeatureRepositoryImpl(this._remoteDataSource, ${hasPersistence ? "this._localDataSource" : ""});

  @override
  Future<void> doSomething(){
    // TODO: implement call
    throw UnimplementedError();
  }
}
''';
