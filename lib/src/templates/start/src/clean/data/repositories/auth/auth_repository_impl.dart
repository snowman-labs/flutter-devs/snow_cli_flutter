import 'package:flutter_snow_blower/src/enums/persistence_enum.dart';

String authRepoImpl(String package, {bool hasPersistence = true}) => '''
import 'dart:async';

import 'package:injectable/injectable.dart';
${ hasPersistence ?
"import 'package:$package/data/data_sources/auth/auth_local_data_source.dart';" : ""}
import 'package:$package/data/data_sources/auth/auth_remote_data_source.dart';
import 'package:$package/data/mappers/index.dart';
import 'package:$package/domain/entities/auth_entity.dart';
import 'package:$package/domain/entities/user_entity.dart';
import 'package:$package/domain/repositories/auth/auth_repository.dart';

@Singleton(as: AuthRepository)
class AuthRepositoryImpl implements AuthRepository {
  final AuthRemoteDataSource _remote;
  ${hasPersistence ? "final AuthLocalDataSource _local;" : ""}
  final _userStream = StreamController<UserEntity>.broadcast();

  AuthRepositoryImpl(this._remote,  ${hasPersistence ? "this._local" : ""});

  @override
  Stream<UserEntity> getUserStream() => _userStream.stream;

  @override
  Future<UserEntity> loginUserEmail(
    String email,
    String password, {
    bool saveAuthInfo = true,
  }) async {
    final userEmail = await _remote.loginUserEmail(email, password);
    if (saveAuthInfo) {
      _local
          .saveAuthInfo(AuthEntity(email: email, password: password).toJson());
    }
    var user = UserEntity(email: userEmail);
    _userStream.add(user);
    return user;
  }

  @override
  Future<void> logoutUser() async {
    await _local.deleteAuthInfo();
    _userStream.add(null);
    return _remote.logoutUser();
  }

  @override
  Future<UserEntity> registerUserEmail(String email, String password) async {
    final userEmail = await _remote.registerUserEmail(email, password);
    _local.saveAuthInfo(AuthEntity(email: email, password: password).toJson());
    var user = UserEntity(email: userEmail);
    _userStream.add(user);
    return user;
  }

  @override
  Future<UserEntity> getUser() async {
    final authInfoString = await _local.getAuthInfo();
    if(authInfoString == null || authInfoString.isEmpty) {
      _userStream.add(null);
      return null;
    }
    final authInfo = AuthEntity().fromJson(authInfoString);
    return loginUserEmail(
      authInfo.email,
      authInfo.password,
      saveAuthInfo: false,
    );
  }
}
''';
