String getUserUseCase(String package) => '''
import 'package:injectable/injectable.dart';
import 'package:$package/domain/entities/user_entity.dart';
import 'package:$package/domain/repositories/auth/auth_repository.dart';
import 'package:$package/domain/usecases/base/base_future_use_case.dart';

@injectable
class GetUserUseCase extends BaseFutureUseCase<void, UserEntity> {
  final AuthRepository _repository;

  GetUserUseCase(this._repository);

  @override
  Future<UserEntity> call([void params]) => _repository.getUser();
}
''';
