String logoutUserUseCase(String package) => '''
import 'package:injectable/injectable.dart';
import 'package:$package/domain/repositories/auth/auth_repository.dart';
import 'package:$package/domain/usecases/base/base_future_use_case.dart';

@injectable
class LogoutUserUseCase extends BaseFutureUseCase<void, void> {
  final AuthRepository _repository;

  LogoutUserUseCase(this._repository);

  @override
  Future<void> call([void params]) => _repository.logoutUser();
}
''';
