String getUserStreamUseCase(String package) => '''
import 'package:injectable/injectable.dart';
import 'package:$package/domain/entities/user_entity.dart';
import 'package:$package/domain/repositories/auth/auth_repository.dart';
import 'package:$package/domain/usecases/base/base_stream_use_case.dart';

@injectable
class GetUserStreamUseCase extends BaseStreamUseCase<void, UserEntity> {
  final AuthRepository _repository;

  GetUserStreamUseCase(this._repository);

  @override
  Stream<UserEntity> call([void params]) => _repository.getUserStream();
}
''';
