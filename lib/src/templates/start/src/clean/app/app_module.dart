String startAppModuleClean(String pkg) => '''
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:$pkg/app/pages/home/home_module.dart';

class AppModule extends Module {
  // here will be any class you want to inject into your project (eg bloc, dependency)
  @override
  final List<Bind> binds = [];

  // here will be the routes of your module
  @override
  final List<ModularRoute> routers = [
        ModuleRoute(
          '/',
          module: HomeModule(),
        ),
      ];
}
  ''';

String startAppModuleCleanComplete(String pkg) => '''
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:$pkg/app/pages/home/home_module.dart';
import 'package:$pkg/app/pages/login/login_module.dart';
import 'package:$pkg/app/pages/splash/splash_module.dart';
import 'package:$pkg/app/stores/auth/auth_store.dart';

class AppModule extends Module {
  // here will be any class you want to inject into your project (eg bloc, dependency)
  @override
  final List<Bind> binds = [
        Bind((i) => AuthStore()),
      ];

  // here will be the routes of your module
  @override
  final List<ModularRoute> routes = [
       ModuleRoute(
          '/',
          module: SplashModule(),
          transition: TransitionType.noTransition,
        ),
       ModuleRoute('/login',
          module: LoginModule(),
        ),
       ModuleRoute('/home',
          module: HomeModule(),
        ),
      ];
}
  ''';
