String startAppWidgetClean(String package) => '''
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_snow_base/flutter_snow_base.dart';
import 'package:$package/app/styles/app_theme_data.dart';

class AppWidget extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return SnowMaterialApp(
      title: 'Flutter Demo',
      theme: AppThemeData.themeDataLight,
      darkTheme: AppThemeData.themeDataDark,
      builder: (context, child) => GestureDetector(
        onTap: () {
          // When running in iOS, dismiss the keyboard when any Tap happens outside a TextField
          if (Platform.isIOS) WidgetUtils.hideKeyboard(context);
        },
        child: child,
      ),
      initialRoute: '/',
    );
  }
}
  ''';

String startAppWidgetCleanWithFlutterIntl(String package) => '''
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_snow_base/flutter_snow_base.dart';
import 'package:$package/app/styles/app_theme_data.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import '../generated/l10n.dart';

class AppWidget extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return SnowMaterialApp(
    localizationsDelegates: [
        S.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: S.delegate.supportedLocales,
      title: 'Flutter Demo',
      theme: AppThemeData.themeDataLight,
      darkTheme: AppThemeData.themeDataDark,
      builder: (context, child) => GestureDetector(
        onTap: () {
          // When running in iOS, dismiss the keyboard when any Tap happens outside a TextField
          if (Platform.isIOS) WidgetUtils.hideKeyboard(context);
        },
        child: child,
      ),
      initialRoute: '/',
    );
  }
}
  ''';

String startAppWidgetCleanComplete(String package) => '''
import 'dart:io';

import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_snow_base/flutter_snow_base.dart';
import 'package:$package/app/styles/app_theme_data.dart';

class AppWidget extends StatelessWidget {
  final FirebaseAnalytics analytics = FirebaseAnalytics();
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return SnowMaterialApp(
      title: 'Flutter Demo',
      theme: AppThemeData.themeDataLight,
      darkTheme: AppThemeData.themeDataDark,
      builder: (context, child) => GestureDetector(
        onTap: () {
          // When running in iOS, dismiss the keyboard when any Tap happens outside a TextField
          if (Platform.isIOS) WidgetUtils.hideKeyboard(context);
        },
        child: child,
      ),
      initialRoute: '/',
      navigatorObservers: [
        FirebaseAnalyticsObserver(analytics: analytics),
      ],
    );
  }
}
  ''';

String startAppWidgetCleanCompleteWithFlutterIntl(String package) => '''
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_snow_base/flutter_snow_base.dart';
import 'package:$package/app/styles/app_theme_data.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import '../generated/l10n.dart';

class AppWidget extends StatelessWidget {
  final FirebaseAnalytics analytics = FirebaseAnalytics();
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return SnowMaterialApp(
      localizationsDelegates: [
          S.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
      supportedLocales: S.delegate.supportedLocales,
      title: 'Flutter Demo',
      theme: AppThemeData.themeDataLight,
      darkTheme: AppThemeData.themeDataDark,
      builder: (context, child) => GestureDetector(
        onTap: () {
          // When running in iOS, dismiss the keyboard when any Tap happens outside a TextField
          if (Platform.isIOS) WidgetUtils.hideKeyboard(context);
        },
        child: child,
      ),
      initialRoute: '/',
      navigatorObservers: [
        FirebaseAnalyticsObserver(analytics: analytics),
      ],
    );
  }
}
  ''';
