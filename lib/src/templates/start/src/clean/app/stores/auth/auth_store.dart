String authStore(String package) => '''
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'package:$package/di/di.dart';
import 'package:$package/domain/entities/auth_status.dart';
import 'package:$package/domain/usecases/auth/get_auth_status_stream_use_case.dart';
part 'auth_store.g.dart';

class AuthStore = _AuthStoreBase with _\$AuthStore;

abstract class _AuthStoreBase with Store {
  final authStatusStream = getIt<GetAuthStatusStreamUseCase>()();

  _AuthStoreBase() {
    authStatusStream.listen(_authListener);
  }

  void _authListener(AuthStatus status) {
    if (status == AuthStatus.login) {
      Modular.to.popUntil((route) => route.isFirst);
      Modular.to.pushReplacementNamed('/home');
    } else if (status == AuthStatus.logoff) {
      Modular.to.popUntil((route) => route.isFirst);
      Modular.to.pushReplacementNamed('/login');
    }
  }
}
''';
