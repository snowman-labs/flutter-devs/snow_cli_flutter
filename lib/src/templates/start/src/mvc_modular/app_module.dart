String startAppModuleModular(String pkg) => '''
import 'package:$pkg/src/shared/clients/dio_client.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:$pkg/src/shared/clients/hive_client.dart';

import 'app_widget.dart';
import 'modules/home/home_module.dart';

class AppModule extends Module {
  @override
  final List<Bind> binds = [
        Bind((i) => DioClient()),
        Bind((i) => HiveClient()),
      ];

  @override
  final List<ModularRoute> routes = [
       ModuleRoute(Modular.initialRoute, module: HomeModule()),
      ];
}
  ''';

String startAppModuleModularComplete(String pkg) => '''
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:$pkg/src/modules/login/login_module.dart';
import 'package:$pkg/src/shared/auth/auth_store.dart';
import 'package:$pkg/src/shared/auth/repositories/auth_repository.dart';
import 'package:$pkg/src/shared/auth/repositories/auth_repository_interface.dart';
import 'package:$pkg/src/shared/clients/dio_client.dart';
import 'package:flutter_snow_base/flutter_snow_base.dart';
import 'package:$pkg/src/shared/clients/hive_client.dart';

import 'app_widget.dart';
import 'modules/home/home_module.dart';
import 'modules/splash/splash_page.dart';

class AppModule extends Module {
  @override
  final List<Bind> binds = [
        Bind((i) => DioClient()),
        Bind<AuthRepository>((i) => AuthRepositoryDefault()),
        Bind((i) => HiveClient()),
        Bind((i) => AuthStore(), singleton: true),
      ];

  @override
  final List<ModularRoute> routes = [
       ModuleRoute(
          Modular.initialRoute,
          child: (context, args) => SplashPage(),
          transition: TransitionType.noTransition,
        ),
       ModuleRoute('/login',
            module: LoginModule()),
       ModuleRoute('/home',
            module: HomeModule()),
      ];
}
  ''';
