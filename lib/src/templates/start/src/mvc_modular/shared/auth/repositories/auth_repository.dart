String authRepo(String package) => '''
import 'package:flutter_modular/flutter_modular.dart';
import 'package:$package/src/shared/models/user_model.dart';
import 'package:$package/src/shared/clients/hive_client.dart';

import 'auth_repository_interface.dart';

class AuthRepositoryDefault implements AuthRepository {
  final HiveClient localStorage = Modular.get();

  @override
  Future<UserModel> loginEmailPassword(String email, String password) async {
    // TODO: implement loginEmailPassword
    final user = UserModel(email: email);
    await _saveUser(user);
    return user;
  }
  
  @override
  Future<UserModel> registerEmailPassword(String email, String password) async {
    // TODO: implement registerEmailPassword
    final user = UserModel(email: email);
    await _saveUser(user);
    return user;
  }

  @override
  Future<UserModel> loginFacebook() {
    // TODO: implement loginFacebook
    throw UnimplementedError();
  }

  @override
  Future<UserModel> loginGoogle() {
    // TODO: implement loginGoogle
    throw UnimplementedError();
  }

  @override
  Future logout() async {
    // TODO: implement logout
    await _saveUser(null);
    return null;
  }

  @override
  Future<String> getToken() {
    // TODO: implement getToken
    throw UnimplementedError();
  }

  @override
  Future<UserModel> getUser() async {
    return _getSavedUser;
  }

  Future<void> _saveUser(UserModel user) {
    return localStorage.put("user", "0", user?.toJson());
  }

  Future<UserModel> get _getSavedUser async {
    final user = await localStorage.get<String>("user", "0");
    if (user == null) return null;
    return UserModel.fromJson(user);
  }
}
''';
