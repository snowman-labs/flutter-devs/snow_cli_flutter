String startMainDevClean(String pkg, String pathToAppModule) => '''
import 'package:flutter_modular/flutter_modular.dart';
import 'package:injectable/injectable.dart';
import 'package:flutter_snow_base/flutter_snow_base.dart';
import 'package:$pkg/data/constants/constants.dart';
import 'package:$pkg/data/helpers/error_mapper.dart';

import '$pathToAppModule/app_module.dart';
import '$pathToAppModule/app_widget.dart';
import 'di/di.dart';

void main() {
  RunAppSnow(
    ModularApp(
      module: AppModule(),
    ),
    flavorValues: Constants.flavorDev,
    flavor: Flavor.dev,
    getItInit: () {
      Resource.setErrorMapper((e) => ErrorMapper.from(e));
      configureInjection(dev.name);
    },
    enableDevicePreview: false,
  );
}
  ''';

String startMainDevModular(String pkg, String pathToAppModule) => '''
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_snow_base/flutter_snow_base.dart';
import 'package:$pkg/$pathToAppModule/app_module.dart';
import 'package:$pkg/$pathToAppModule/app_widget.dart';
import 'package:$pkg/$pathToAppModule/shared/constants/constants.dart';
import 'package:$pkg/$pathToAppModule/shared/helpers/error_mapper.dart';

void main() {
  RunAppSnow(
    ModularApp(
      module: AppModule(),
      child: AppWidget(),
    ),
    flavorValues: Constants.flavorDev,
    getItInit: () => Resource.setErrorMapper((e) => ErrorMapper.from(e)),
    flavor: Flavor.dev,
    enableDevicePreview: false,
  );
}
  ''';
