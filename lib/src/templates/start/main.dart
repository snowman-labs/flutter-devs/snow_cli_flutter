String startMain(String pkg, String pathToAppModule) => '''
import 'package:flutter/material.dart';
import 'package:$pkg/$pathToAppModule/app_module.dart';

void main() => runApp(AppModule());
  ''';

String startMainModular(String pkg, String pathToAppModule) => '''
import 'package:flutter/material.dart';
import 'package:$pkg/$pathToAppModule/app_module.dart';
import 'package:$pkg/$pathToAppModule/app_widget.dart';
import 'package:flutter_modular/flutter_modular.dart';

void main() => runApp(ModularApp(module: AppModule(), child: AppWidget()));
  ''';
