Future<String> _getVersion() async {
  return '2021.2.0+1';
}

void version() async {
  printSnowBlower();
  print('CLI package manager and template for Flutter');
  print('');
  print("Flutter Snow Blower CLI version: ${await _getVersion()}");
}

void printSnowBlower() {
  print('''
                                                      %                           
                                                   %  %                         
                                                                                
%%%%%%%%%%%%%%                                     %  % %  %                    
%%(((........%.                                                                 
        %....*%                               %    %  % %  % %  %               
        #%....%%                            (((%%                               
         %%....%%                        *(((****%%%                            
          %%....%%                     (((****,   *%%                           
           %%....%%                  (((****   *%%%                             
            %%....%%               (((****  **%%                                
             %,....%%             ((****   *%%                                  
             ,%.....%.           ((*****  #%                                    
              %%....*%           ((******.*%%                          %%%%%%%%%
                ..................((*********%%%                   %%%##(((((((%
                 ....................*************%%%%%            %%##((((((((%
                %%%################################################&%##((((((((%
                %%%################################################&%##((((((((%
                %%%################################################&%##(((((  (%
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%&%###((((  (%
                &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&%%###((((  (%
                @&&((((((&&....&&#(((((&&&&&(((((((&/....&#((((((&& %%##((((  (%
                 &&&((((((((&&#((((####&& &&&((((((((&&&((((((((&&&  %%##(((  (%
                  &&&((((((((((((((((&&&%%% &&%((((((((((((((((&&/     %%#((  (%
                    &&&&((((((((((&&&&      %%&&&#((((((((((&&&%         %%%%((%
                        &&&&&&&&&&%              &&&&&&&&&&&                   %
                                                                                                             
                                                               
       _____                          ______  _                              
      /  ___|                         | ___ \\| |                             
      \\ `--.  _ __    ___  __      __ | |_/ /| |  ___  __      __  ___  _ __ 
       `--. \\| '_ \\  / _ \\ \\ \\ /\\ / / | ___ \\| | / _ \\ \\ \\ /\\ / / / _ \\| '__|
      /\\__/ /| | | || (_) | \\ V  V /  | |_/ /| || (_) | \\ V  V / |  __/| |   
      \\____/ |_| |_| \\___/   \\_/\\_/   \\____/ |_| \\___/   \\_/\\_/   \\___||_|   
                                                                       
                                                                       
''');
}
