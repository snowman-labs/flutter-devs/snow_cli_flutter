part of './create_mvc_modular.dart';

Future<void> _createPagesFiles(String package, bool complete,
{Internationalization internationalization = Internationalization.i18n}) async {
  await Generate.module('modules/home', true, false);

  if (internationalization == Internationalization.i18n) {
    createStaticFile(
      libPath('i18n/validators_i18n.dart'),
      templates.validatorsI18n(),
    );
  }

  if (complete) {
    if (internationalization == Internationalization.flutterIntl) {
      createStaticFile(
        libPath('modules/splash/splash_page.dart'),
        templates.splashPageModularWithFlutterIntl(package),
      );
    } else {
      createStaticFile(
        libPath('modules/splash/splash_page.dart'),
        templates.splashPageModular(package),
      );
    }

    // ** login
    createStaticFile(
      libPath('modules/login/login_module.dart'),
      templates.loginModule(),
    );
    createStaticFile(
      libPath('modules/login/pages/login/login_page.dart'),
      internationalization == Internationalization.i18n
          ? templates.loginPage(package, Architecture.mvcModular)
          : templates.loginPageWithFlutterIntl(
              package, Architecture.mvcModular),
    );
    createStaticFile(
      libPath('modules/login/pages/login/login_controller.dart'),
      templates.loginControllerModular(package),
    );

    // i18n
    if (internationalization == Internationalization.i18n) {
      createStaticFile(
        libPath('i18n/login_i18n.dart'),
        templates.loginI18n(),
      );
    }

    // widget
    createStaticFile(
      libPath('shared/widgets/email_text_field/email_text_field_widget.dart'),
      templates.emailTextFieldWidget(package, "src", Architecture.mvcModular),
    );
    createStaticFile(
      libPath(
          'shared/widgets/password_text_field/password_text_field_widget.dart'),
      templates.passwordTextFieldWidget(
          package, "src", Architecture.mvcModular),
    );

    // ** register
    createStaticFile(
      libPath('modules/login/pages/register/register_page.dart'),
      internationalization == Internationalization.i18n
          ? templates.registerPage(package, Architecture.mvcModular)
          : templates.registerPageWithFlutterIntl(
              package, Architecture.mvcModular),
    );
    createStaticFile(
      libPath('modules/login/pages/register/register_controller.dart'),
      templates.registerControllerModular(package),
    );
  }
}
