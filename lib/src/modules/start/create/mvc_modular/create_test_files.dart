part of './create_mvc_modular.dart';

void _createTestFiles(String package, bool complete, String dirPath) {
  dirPath = dirPath.replaceFirst('lib', '');
  createStaticFile(
    '${dirPath}test/src/shared/clients/dio_client_test.dart',
    templates.dioClientTest(package, Architecture.mvcModular),
  );
  createStaticFile(
    '${dirPath}test/test_variants/flavors_test_variant.dart',
    templates.flavorsTestVariant(package, Architecture.mvcModular),
  );
  createStaticFile(
    '${dirPath}test/src/modules/drawer_features_example_test.dart',
    templates.drawerFeaturesExampleTest(package, Architecture.mvcModular),
  );
  if (complete) {
    createStaticFile(
      '${dirPath}test/src/i18n/i18n_test.dart',
      templates.i18nTest(package, Architecture.mvcModular),
    );
  }
}
