part of './create_mvc_modular.dart';

void _createSharedFiles(String package, bool complete,
    {Internationalization internationalization = Internationalization.i18n}) {
  // ** shared
  createStaticFile(
    libPath('shared/extensions/extensions.dart'),
    templates.extensions(),
  );

  createStaticFile(
    libPath('shared/constants/constants.dart'),
    templates.constants(package),
  );
  createStaticFile(
    libPath('shared/constants/features.dart'),
    templates.features(),
  );

  // Helpers
  createStaticFile(
    libPath('shared/helpers/error_mapper.dart'),
    templates.errorMapper(),
  );

  if (internationalization == Internationalization.i18n) {
    createStaticFile(
      libPath('shared/helpers/validators.dart'),
      templates.validators(Architecture.mvcModular),
    );
  }

  createStaticFile(
    libPath('shared/clients/hive_client.dart'),
    templates.hiveClient(package, false),
  );
  createStaticFile(
    libPath('shared/clients/dio_client.dart'),
    templates.dioClient(package, false, Architecture.mvcModular),
  );

  if (complete) {
    createStaticFile(
      libPath('shared/models/user_model.dart'),
      templates.userModel(),
    );
    createStaticFile(
      libPath('shared/auth/auth_store.dart'),
      templates.authStoreModular(package),
    );
    createStaticFile(
      libPath('shared/auth/repositories/auth_repository_interface.dart'),
      templates.authRepoInterface(package),
    );
    createStaticFile(
      libPath('shared/auth/repositories/auth_repository.dart'),
      templates.authRepo(package),
    );
  }
}
