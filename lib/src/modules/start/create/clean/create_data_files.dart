part of './create_clean_architecture.dart';

void _createDataFiles(String package, bool complete, Persistence persistence) {
  //Constants
  createStaticFile(
    libPath('data/constants/constants.dart'),
    templates.constants(package),
  );
  //Features
  createStaticFile(
    libPath('data/constants/features.dart'),
    templates.features(),
  );
  //Extensions
  createStaticFile(
    libPath('data/helpers/extensions.dart'),
    templates.extensions(),
  );
  //ErrorMapper
  createStaticFile(
    libPath('data/helpers/error_mapper.dart'),
    templates.errorMapper(),
  );

  if (persistence == Persistence.moor ||
      persistence == Persistence.hiveAndMoor) {
    //Moor Db
    createStaticFile(
      libPath('data/local/moor_db.dart'),
      templates.moorDb(package, true),
    );
  }
  if (persistence == Persistence.hive ||
      persistence == Persistence.hiveAndMoor) {
    //HiveClient
    createStaticFile(
      libPath('data/local/hive_client.dart'),
      templates.hiveClient(package, true),
    );
  }

  //DioClient
  createStaticFile(
    libPath('data/remote/dio_client.dart'),
    templates.dioClient(package, true, Architecture.cleanArchitecture),
  );

  //MapperIndex
  createStaticFile(
    libPath('data/mappers/index.dart'),
    templates.indexMapper(complete),
  );

  //Moor
  if (persistence == Persistence.moor ||
      persistence == Persistence.hiveAndMoor) {
    //Dao
    createStaticFile(
      libPath('data/local/dao/some_dao.dart'),
      templates.someDao(package, true),
    );
    //Table
    createStaticFile(
      libPath('data/model/local/some_table.dart'),
      templates.someTable(),
    );
  }

  if (complete) {
    //AuthLocalDataSource
    if (persistence != null) {
      createStaticFile(
        libPath('data/data_sources/auth/auth_local_data_source.dart'),
        templates.authLocalDataSource(),
      );
    }
    //AuthRemoteDataSource
    createStaticFile(
      libPath('data/data_sources/auth/auth_remote_data_source.dart'),
      templates.authRemoteDataSource(),
    );
    //AuthMapper
    createStaticFile(
      libPath('data/mappers/auth_mapper.dart'),
      templates.authMapper(package),
    );
    //UserMapper
    createStaticFile(
      libPath('data/mappers/user_mapper.dart'),
      templates.userMapper(package),
    );
    //AuthRepositoryImpl
    createStaticFile(
      libPath('data/repositories/auth/auth_repository_impl.dart'),
      templates.authRepoImpl(package, hasPersistence: persistence != null),
    );
  } else {
    //DataSources
    if (persistence != null) {
      createStaticFile(
        libPath('data/data_sources/feature/feature_local_data_source.dart'),
        templates.featureLocalDataSource(package, persistence),
      );
    }
    createStaticFile(
      libPath('data/data_sources/feature/feature_remote_data_source.dart'),
      templates.featureRemoteDataSource(package),
    );
    //UserMapper
    createStaticFile(
      libPath('data/mappers/user_mapper.dart'),
      templates.userMapper(package),
    );
    //FeatureRepositoryImpl
    createStaticFile(
      libPath('data/repositories/feature/feature_repository_impl.dart'),
      templates.featureRepoImpl(package, hasPersistence: persistence != null),
    );
  }
}
