part of './create_clean_architecture.dart';

void _createAppFiles(String package, bool complete,
    {Internationalization internationalization = Internationalization.i18n}) {
  createStaticFile(
    libPath('app/app_module.dart'),
    complete
        ? templates.startAppModuleCleanComplete(package)
        : templates.startAppModuleClean(package),
  );
  createStaticFile(
    libPath('app/app_widget.dart'),
    complete
        ? internationalization == Internationalization.flutterIntl
            ? templates.startAppWidgetCleanCompleteWithFlutterIntl(package)
            : templates.startAppWidgetCleanComplete(package)
        : internationalization == Internationalization.flutterIntl
            ? templates.startAppWidgetCleanWithFlutterIntl(package)
            : templates.startAppWidgetClean(package),
  );

  if (complete) {
    createStaticFile(
      libPath('app/stores/auth/auth_store.dart'),
      templates.authStore(package),
    );
  }
}
