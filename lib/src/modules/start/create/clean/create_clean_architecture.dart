import 'package:flutter_snow_blower/src/enums/architecture_enum.dart';
import 'package:flutter_snow_blower/src/enums/internationalization.dart';
import 'package:flutter_snow_blower/src/enums/persistence_enum.dart';
import 'package:flutter_snow_blower/src/modules/generate.dart';
import 'package:flutter_snow_blower/src/modules/start/create/shared/create_flavor_values_file.dart';
import 'package:flutter_snow_blower/src/modules/start/create/shared/create_widgets.dart';
import 'package:flutter_snow_blower/src/templates/templates.dart' as templates;
import 'package:flutter_snow_blower/src/utils/file_utils.dart';
import 'package:flutter_snow_blower/src/utils/utils.dart';

part './create_app_files.dart';
part './create_data_files.dart';
part './create_di_files.dart';
part './create_domain_files.dart';
part './create_pages_files.dart';
part './create_test_files.dart';

Future<void> createCleanArchitectureStartFiles(
    String package,
    bool complete,
    String dirPath,
    Persistence persistence,
    Internationalization internationalization) {
  createAppFlavorValuesFile(dirPath, true);
  createWidgetsStartFiles('app/widgets/');
  _createAppFiles(package, complete,
      internationalization: internationalization);
  _createDataFiles(package, complete, persistence);
  _createDiFiles(package, complete);
  _createDomainFiles(package, complete);
  _createTestFiles(package, complete, dirPath);
  return _createPagesFiles(package, complete,
      internationalization: internationalization);
}
