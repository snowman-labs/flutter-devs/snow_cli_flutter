import 'package:flutter_snow_blower/src/enums/internationalization.dart';
import 'package:flutter_snow_blower/src/templates/templates.dart' as templates;
import 'package:flutter_snow_blower/src/utils/file_utils.dart';
import 'package:flutter_snow_blower/src/utils/utils.dart';

void createWidgetsStartFiles(String widgetsPath,
    [Internationalization internationalization]) {
  createStaticFile(
    libPath('${widgetsPath}custom_alert_dialog/custom_alert_dialog.dart'),
    templates.customAlertDialog(),
  );
  createStaticFile(
    libPath('${widgetsPath}custom_alert_dialog/types/error_dialog.dart'),
    templates.errorDialogType(),
  );
  createStaticFile(
    libPath('${widgetsPath}custom_alert_dialog/types/confirm_dialog.dart'),
    templates.confirmDialogType(),
  );
}
