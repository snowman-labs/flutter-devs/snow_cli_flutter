part of './../create.dart';

void _createStyleFiles(String stylesPath) {
  createStaticFile(
    libPath('$stylesPath/app_bar_theme_app.dart'),
    templates.appBarThemeApp(),
  );
  createStaticFile(
    libPath('$stylesPath/app_color_scheme.dart'),
    templates.appColorSchemeApp(),
  );
  createStaticFile(
    libPath('$stylesPath/app_text_theme.dart'),
    templates.appTextThemeApp(),
  );
  createStaticFile(
    libPath('$stylesPath/app_theme_data.dart'),
    templates.appThemeDataApp(),
  );
}
