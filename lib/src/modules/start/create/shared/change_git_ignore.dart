import 'dart:io';

Future<void> changeGitIgnore(
    String workingDirectory, String filesToAdd, String fileToBeReplaced) async {
  final file = File('$workingDirectory.gitignore');
  var contents = await file.readAsString();
  final lines = file.readAsLinesSync();
  if (lines.contains(fileToBeReplaced)) {
    contents = contents.replaceFirst(fileToBeReplaced, filesToAdd);
  }
  return file.writeAsString(contents);
}
