import 'package:flutter_snow_blower/src/enums/architecture_enum.dart';
import 'package:flutter_snow_blower/src/enums/internationalization.dart';
import 'package:flutter_snow_blower/src/enums/persistence_enum.dart';
import 'package:flutter_snow_blower/src/modules/start/create/clean/create_clean_architecture.dart';
import 'package:flutter_snow_blower/src/modules/start/create/mvc_modular/create_mvc_modular.dart';
import 'package:flutter_snow_blower/src/modules/start/create/shared/change_git_ignore.dart';
import 'package:flutter_snow_blower/src/templates/templates.dart' as templates;
import 'package:flutter_snow_blower/src/utils/file_utils.dart';
import 'package:flutter_snow_blower/src/utils/utils.dart';

part './shared/create_android_studio_settings.dart';
part './shared/create_main_files.dart';
part './shared/create_style_files.dart';
part './shared/create_vs_code_settings.dart';

Future<void> createStartFiles(
    Architecture architecture,
    String dirPath,
    bool completeStart,
    bool useEnvironment,
    Persistence persistence,
    String package,
    {Internationalization internationalization}) async {
  // Set architecture to libPath
  libPath('', architecture: architecture);

  _createVsCodeSettings(package);

  _createAndroidStudioSettings(package);

  switch (architecture) {
    case Architecture.cleanArchitecture:
      _createMainFile(
        dirPath,
        package,
        useEnviroment: useEnvironment,
        pathToAppModule: 'app',
        architecture: architecture,
      );

      _createStyleFiles('app/styles');

      await createCleanArchitectureStartFiles(
          package, completeStart, dirPath, persistence, internationalization);
      break;
    case Architecture.mvcModular:
      _createMainFile(
        dirPath,
        package,
        useEnviroment: useEnvironment,
        pathToAppModule: 'src',
        architecture: architecture,
      );

      _createStyleFiles('styles');

      await createMvcModularStartFiles(
          package, completeStart, dirPath, internationalization);
      break;
  }
}
