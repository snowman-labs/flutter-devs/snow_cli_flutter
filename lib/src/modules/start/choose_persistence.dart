import 'package:flutter_snow_blower/src/enums/persistence_enum.dart';

import 'select_option.dart';

Future<Persistence> choosePersistence() async {
  var selected = selectOption(
    'What persistence do you want to use?',
    [
      'Hive (default)',
      'Moor',
      'Hive + Moor',
      'I don\'t want to use persistence'
    ],
  );

  switch (selected) {
    case 0:
      return Persistence.hive;
    case 1:
      return Persistence.moor;
    case 2:
      return Persistence.hiveAndMoor;
    default:
      return null;
  }
}
