import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter_snow_blower/flutter_snow_blower.dart';
import 'package:flutter_snow_blower/src/enums/architecture_enum.dart';
import 'package:flutter_snow_blower/src/enums/internationalization.dart';
import 'package:flutter_snow_blower/src/enums/persistence_enum.dart';
import 'package:flutter_snow_blower/src/modules/install.dart';
import 'package:flutter_snow_blower/src/modules/start/choose_internationalization.dart';
import 'package:flutter_snow_blower/src/modules/start/choose_persistence.dart';
import 'package:flutter_snow_blower/src/modules/start/create/create.dart';
import 'package:flutter_snow_blower/src/modules/start/delete_lib.dart';
import 'package:flutter_snow_blower/src/modules/start/select_architecture.dart';
import 'package:flutter_snow_blower/src/utils/output_utils.dart' as output;
import 'package:flutter_snow_blower/src/utils/pubspec.dart';
import 'package:flutter_snow_blower/src/utils/utils.dart';

import 'change_min_sdk_version_build_gradle.dart';

part './install_packages.dart';

Future<void> start({
  bool completeStart,
  bool force = false,
  Directory dir,
  Architecture architecture,
  Persistence persistence,
  Internationalization internationalization,
  bool useEnvironment = true,
  Future<Process> Function() createProject,
}) async {
  dir ??= Directory('lib');
  var workingDirectory = dir.path.replaceAll('lib', '');

  architecture ??= await selectArchitecture();
  persistence ??= await choosePersistence();
  internationalization ??= await chooseInternationalization();

  await deleteLib(dir, !dir.parent.existsSync());

  if (createProject != null) {
    final process = await createProject();
    await stdout.addStream(process.stdout);
    await stderr.addStream(process.stderr);
    final exitCode = await process.exitCode;
    if (exitCode != 0) {
      output.error('Falha na criação do projeto Flutter');
      exit(1);
    }
  }

  await deleteLib(dir, force);

  var dirTest = Directory(dir.parent.path + '/test');
  if (await dirTest.exists()) {
    if (dirTest.listSync().isNotEmpty) {
      output.msg('Removing test folder');
      await dirTest.delete(recursive: true);
    }
  }

  var package = await getNamePackage(dir.parent);

  await Future.wait<void>([
    _installPackages(dir.parent.path, completeStart, architecture, persistence,
        workingDirectory,
        internationalization: internationalization),
    createStartFiles(architecture, dir.path, completeStart, useEnvironment,
        persistence, package,
        internationalization: internationalization),
  ]);

  try {
    print('Replacing pubspec.yaml...');
    PubSpec().replacePubSpecYaml(workingDirectory, internationalization);
  } on Exception {
    print('Replacing failed. Using the original version.');
  }

  await _getPackages(workingDirectory);
  await _buildRunnerBuild(workingDirectory);
  if (internationalization == Internationalization.flutterIntl) {
    await _runIntlUtils(workingDirectory);
  }

  printSnowBlower();
  if (internationalization == Internationalization.flutterIntl) {
    output.msg(
        "Please see the Flutter Intl documentation at: https://plugins.jetbrains.com/plugin/13666-flutter-intl");
  }
  output.msg('\n\n            Project started! enjoy!');
}

Future<void> _getPackages(String workingDirectory) async {
  final finished = Completer<bool>();
  final process = await Process.start(
    'flutter',
    ['packages', 'get'],
    runInShell: true,
    workingDirectory: workingDirectory.isEmpty ? null : workingDirectory,
  );
  process.stdout.transform(utf8.decoder).listen(
        print,
        cancelOnError: true,
        onDone: () => finished.complete(true),
        onError: (e) => finished.complete(true),
      );
  return finished.future;
}

Future<void> _buildRunnerBuild(String workingDirectory) async {
  final finished = Completer<bool>();
  final process = await Process.start(
    'flutter',
    ['pub', 'run', 'build_runner', 'build'],
    runInShell: true,
    workingDirectory: workingDirectory.isEmpty ? null : workingDirectory,
  );
  process.stdout.transform(utf8.decoder).listen(
        print,
        cancelOnError: true,
        onDone: () => finished.complete(true),
        onError: (e) => finished.complete(true),
      );
  return finished.future;
}

Future<void> _runIntlUtils(String workingDirectory) async {
  final finished = Completer<bool>();
  final process = await Process.start(
    'flutter',
    ['pub', 'run', 'intl_utils:generate'],
    runInShell: true,
    workingDirectory: workingDirectory.isEmpty ? null : workingDirectory,
  );
  process.stdout.transform(utf8.decoder).listen(
        print,
        cancelOnError: true,
        onDone: () => finished.complete(true),
        onError: (e) => finished.complete(true),
      );
  return finished.future;
}
