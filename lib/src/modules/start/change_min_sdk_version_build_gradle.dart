import 'dart:io';

Future<void> changeMinSdkVersionBuildGradle(
    String workingDirectory, String version) async {
  final file = File('$workingDirectory/android/app/build.gradle');
  var contents = await file.readAsString();
  contents = contents.replaceFirst('16', version);
  return file.writeAsString(contents);
}
