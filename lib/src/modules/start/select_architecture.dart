import 'dart:io';

import 'package:flutter_snow_blower/src/enums/architecture_enum.dart';
import 'package:flutter_snow_blower/src/modules/start/select_option.dart';

Future<Architecture> selectArchitecture() async {
  var selected = selectOption(
    'What Architecture do you want to use?',
    ['Clean architecture (default)', 'MVC Modular'],
  );

  switch (selected) {
    case 0:
      return Architecture.cleanArchitecture;
    case 1:
      return Architecture.mvcModular;
    default:
      exit(1);
      return null;
  }
}
