part of './start.dart';

Future<void> _installPackages(String directory, bool completeStart, Architecture architecture,
    Persistence persistence, String workingDirectory,
    {Internationalization internationalization}) async {
  await removeAllPackages(directory);

  switch (architecture) {
    case Architecture.cleanArchitecture:
      output.msg('Instaling Clean architecture...');

      await install(['injectable_generator'], true, directory: directory);
      await install(['get_it', 'injectable'], false, directory: directory);

      if (completeStart) {
        await changeMinSdkVersionBuildGradle(
            workingDirectory, '19'); //  -- flutter_secure_storage needs it
        await install(
          [
            'flutter_secure_storage',
            'firebase_auth',
            'firebase_analytics',
            'firebase_core',
          ],
          false,
          directory: directory,
        );
      } else {
        await install(
          ['firebase_core'],
          false,
          directory: directory,
        );
      }
      break;
    case Architecture.mvcModular:
      output.msg('Instaling Modular...');

      break;
  }

  if (persistence == Persistence.moor || persistence == Persistence.hiveAndMoor) {
    await install(['moor'], false, directory: directory);
    await install(['moor_generator'], true, directory: directory);
  }

  if (persistence == Persistence.hive || persistence == Persistence.hiveAndMoor) {
    await install(['hive'], false, directory: directory);
  }

  await install(
    ['mockito', 'mobx_codegen', 'build_runner'],
    true,
    directory: directory,
  );

  /*await install(
    ['analyzer'],
    false,
    isOverride: true,
    directory: directory,
  );*/

  if (internationalization == Internationalization.flutterIntl) {
    await install(['intl_utils'], false, directory: directory);
  } else {
    await install(['i18n_extension'], false, directory: directory);
  }

  await install(
    [
      'path_provider',
      'flutter_modular',
      'dio',
      'string_validator',
      'password_strength',
      'flutter_snow_base',
      'google_fonts',
      'flutter_mobx',
      'mobx'
    ],
    false,
    directory: directory,
  );
  
  await install(
    ['flutter_modular_test'],
    true,
    directory: directory,
  );
}
