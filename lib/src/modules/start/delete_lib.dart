import 'dart:io';

import 'package:flutter_snow_blower/src/modules/start/select_confirm_delete_lib.dart';
import 'package:flutter_snow_blower/src/utils/output_utils.dart' as output;

Future<void> deleteLib(Directory dir, [bool autoConfirm]) async {
  if (await dir.exists() && dir.listSync().isNotEmpty) {
    if (selectConfirmDeleteLib(autoConfirm)) {
      output.msg('Removing lib folder');
      await dir.delete(recursive: true);
    } else {
      output.error('The lib folder must be empty');
      exit(1);
    }
  }
}
