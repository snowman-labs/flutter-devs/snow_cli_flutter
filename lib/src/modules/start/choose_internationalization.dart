import 'package:flutter_snow_blower/src/enums/internationalization.dart';
import 'package:flutter_snow_blower/src/modules/start/select_option.dart';

Future<Internationalization> chooseInternationalization() async {
  var selected = selectOption(
    'What internationalization package do you want to use?',
    ['Flutter Intl (default)', 'i18n'],
  );

  switch (selected) {
    case 0:
      return Internationalization.flutterIntl;
    case 1:
      return Internationalization.i18n;
    default:
      return Internationalization.flutterIntl;
  }
}
