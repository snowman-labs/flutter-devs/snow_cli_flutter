import 'dart:io';

import 'package:args/command_runner.dart';
import 'package:flutter_snow_blower/src/enums/architecture_enum.dart';
import 'package:flutter_snow_blower/src/enums/data_source_enum.dart';
import 'package:flutter_snow_blower/src/enums/internationalization.dart';
import 'package:flutter_snow_blower/src/enums/state_management_enum.dart';
import 'package:flutter_snow_blower/src/templates/generator/prompts/choose_data_source.dart';
import 'package:flutter_snow_blower/src/templates/templates.dart' as templates;
import 'package:flutter_snow_blower/src/utils/file_utils.dart' as file_utils;
import 'package:flutter_snow_blower/src/utils/object_generate.dart';
import 'package:flutter_snow_blower/src/utils/output_utils.dart' as output;
import 'package:flutter_snow_blower/src/utils/utils.dart';
import 'package:path/path.dart';

import '../utils/utils.dart';

class Generate {
  static Future<void> module(
      String path,
      bool createCompleteModule,
      bool noroute,
      ) async {
    var moduleType = createCompleteModule ? 'module_complete' : 'module';
    var templateModular = noroute
        ? templates.moduleGeneratorModularNoRoute
        : templates.moduleGeneratorModular;
    final architecture = projectArchitecture();

    var internationalization = projectInternationalization();

    if (!path.contains('app/') &&
        architecture == Architecture.cleanArchitecture) {
      path = 'app/' + path;
    }

    await file_utils.createFile(path, moduleType, templateModular);
    if (createCompleteModule) {
      await page(path, false,
          stateManagement: defaultStateManagement,
          internationalization: internationalization,
          architecture: architecture);
    }
  }

  static Future<void> page(
    String path,
    bool controllerLess, {
    bool i18nLess = false,
    Architecture architecture,
    Internationalization internationalization,
    StateManagement stateManagement = defaultStateManagement,
  }) async {
    architecture ??= projectArchitecture();
    internationalization ??= projectInternationalization();

    if (!path.contains('app/') &&
        architecture == Architecture.cleanArchitecture) {
      path = 'app/' + path;
    }

    await file_utils.createFile(
      '$path',
      'page',
      stateManagement == StateManagement.mobx
          ? templates.pageGeneratorMobX
          : templates.pageGenerator,
      generatorTest: templates.pageTestGenerator,
      architecture: architecture,
      stateManagement: stateManagement,
    );
    var name = basename(path);
    if (!controllerLess) {
      var type = 'controller';
      await controller('$path/$name', type);
    }
    if (internationalization == Internationalization.i18n) {
      if (!i18nLess) {
        await i18n(path);
      }
    }
  }

  static Future<void> widget(
    String path,
    bool withController, {
    StateManagement stateManagement = defaultStateManagement,
    Architecture architecture,
  }) async {
    architecture ??= projectArchitecture();

    if (!path.contains('app/') &&
        architecture == Architecture.cleanArchitecture) {
      path = 'app/' + path;
    }

    await file_utils.createFile(
      '$path',
      'widget',
      templates.widgetGenerator,
      generatorTest: templates.widgetTestGenerator,
      stateManagement: stateManagement,
      architecture: architecture,
    );

    var name = basename(path);
    if (withController) {
      var type = 'controller';

      return controller('$path/$name', type, haveTest: true);
    }
  }

  static Future<void> i18n(String name,
      [bool tests = true, bool modular]) async {
    var suffix = '';
    if (existsFile('app')) {
      suffix = 'app/';
    } else if (existsFile('src')) {
      suffix = 'src/';
    }
    await file_utils.createFile(
      "$suffix/i18n/$name",
      'i18n',
      templates.i18nGenerator,
      generatorTest: tests ? templates.i18nGeneratorTest : null,
    );
  }

  static Future<void> test(String path) async {
    final architecture = projectArchitecture();

    if (!path.contains('app/') &&
        architecture == Architecture.cleanArchitecture) {
      path = 'app/' + path;
    }

    if (path.contains('.dart')) {
      var entity = File(libPath(path));
      if (!entity.existsSync()) {
        output.error('File $path not exist');
        exit(1);
      }
      return _generateTest(
          entity,
          File(libPath(path)
              .replaceFirst('lib/', 'test/')
              .replaceFirst('.dart', '_test.dart')));
    } else {
      var entity = Directory(libPath(path));
      if (!entity.existsSync()) {
        output.error('Directory $path not exist');
        exit(1);
      }

      for (var file in entity.listSync()) {
        if (file is File) {
          await _generateTest(
            file,
            File(
              file.path
                  .replaceFirst('lib/', 'test/')
                  .replaceFirst('.dart', '_test.dart'),
            ),
          );
        }
      }
    }
  }

  static Future<void> _generateTest(File entity, File entityTest) async {
    if (entityTest.existsSync()) {
      output.error('Test already exists');
      exit(1);
    }

    final name = basename(entity.path);
    final module = file_utils.findModule(entity.path);
    final nameModule = module == null ? null : basename(module.path);

    if (name.contains('_repository.dart')) {
      entityTest.createSync(recursive: true);
      output.msg('File test ${entityTest.path} created');
      entityTest.writeAsStringSync(
        templates.repositoryTestGenerator(
          ObjectGenerate(
            name: formatName(name.replaceFirst('_repository.dart', '')),
            packageName: await getNamePackage(),
            import: entity.path,
            module: nameModule == null ? null : formatName(nameModule),
            pathModule: module?.path,
          ),
        ),
      );
    } else if (name.contains('_page.dart')) {
      entityTest.createSync(recursive: true);
      output.msg('File test ${entityTest.path} created');
      entityTest.writeAsStringSync(
        templates.pageTestGenerator(
          ObjectGenerate(
            name: formatName(name.replaceFirst('_page.dart', '')),
            packageName: await getNamePackage(),
            import: entity.path,
            module: nameModule == null ? null : formatName(nameModule),
            pathModule: module?.path,
          ),
        ),
      );
    } else if (name.contains('_controller.dart')) {
      entityTest.createSync(recursive: true);
      output.msg('File test ${entityTest.path} created');
      entityTest.writeAsStringSync(
        templates.mobxControllerTestGeneratorModular(
          ObjectGenerate(
            name: formatName(name.replaceFirst('_controller.dart', '')),
            type: 'controller',
            packageName: await getNamePackage(),
            import: entity.path,
            module: nameModule == null ? null : formatName(nameModule),
            pathModule: module?.path ?? "",
          ),
        ),
      );
    }
  }

  static Future<void> repository(
    String path, {
    bool haveTest = true,
    bool complete = false,
  }) async {
    final architecture = projectArchitecture();
    final name = basename(path);

    switch (architecture) {
      case Architecture.cleanArchitecture:
        if (complete) {
          await dataSource(name);
        }

        await file_utils.createFile(
          'domain/repositories/$name/$name',
          'repository',
          templates.repositoryGeneratorClean,
          generatorTest: null,
        );
        await file_utils.createFile(
          'data/repositories/$name',
          'repository_impl',
          templates.repositoryImplGeneratorClean,
          generatorTest:
              haveTest ? templates.repositoryImplTestGeneratorClean : null,
        );
        break;

      case Architecture.mvcModular:
      default:
        await file_utils.createFile(
          path,
          'repository',
          templates.repositoryGeneratorModular,
          generatorTest: haveTest ? templates.repositoryTestGenerator : null,
        );
    }
  }

  static Future<void> controller(String path, String type,
      {bool haveTest = true}) async {
    final template = templates.mobxControllerGenerator;
    final testTemplate = templates.mobxControllerTestGeneratorModular;
    final architecture = projectArchitecture();

    if (!path.contains('app/') &&
        architecture == Architecture.cleanArchitecture) {
      path = 'app/' + path;
    }

    await file_utils.createFile(
      path,
      type,
      template,
      architecture: architecture,
      generatorTest: haveTest ? testTemplate : null,
    );
  }

  static Future<void> dataSource(String name,
      {bool haveTest = true, String usage = ''}) async {
    final architecture = projectArchitecture();

    if (architecture != Architecture.cleanArchitecture) {
      throw UsageException(
        'Your project needs to be with Clean Architecture to run this command.',
        usage,
      );
    }

    var dataSource = await chooseDataSource();

    if(dataSource == DataSource.onlyLocal){
      await file_utils.createFile(
        'data/data_sources/$name',
        'local_data_source',
        templates.localDataSourceGenerator,
        architecture: architecture,
        generatorTest: haveTest ? templates.localDataSourceGeneratorTest : null,
      );
    }else if(dataSource == DataSource.onlyRemote){
      await file_utils.createFile(
        'data/data_sources/$name',
        'remote_data_source',
        templates.remoteDataSourceGenerator,
        architecture: architecture,
        generatorTest: haveTest ? templates.remoteDataSourceGeneratorTest : null,
      );
    }else {
      await file_utils.createFile(
        'data/data_sources/$name',
        'local_data_source',
        templates.localDataSourceGenerator,
        architecture: architecture,
        generatorTest: haveTest ? templates.localDataSourceGeneratorTest : null,
      );
      await file_utils.createFile(
        'data/data_sources/$name',
        'remote_data_source',
        templates.remoteDataSourceGenerator,
        architecture: architecture,
        generatorTest: haveTest ? templates.remoteDataSourceGeneratorTest : null,
      );
    }
  }

  static Future<void> entity(String name, {String usage = ''}) async {
    final architecture = projectArchitecture();

    if (architecture != Architecture.cleanArchitecture) {
      throw UsageException(
        'Your project needs to be with Clean Architecture to run this command.',
        usage,
      );
    }

    final haveEquatable = await checkDependency('equatable');
    await file_utils.createFile(
      'domain/entities/$name',
      'entity',
      templates.entityGenerator,
      generatorTest: null,
      addicionalInfo: haveEquatable,
      architecture: architecture,
    );
    await file_utils.createFile(
      'data/mappers/$name',
      'mapper',
      templates.entityMapperGenerator,
      generatorTest: null,
      addicionalInfo: haveEquatable,
      architecture: architecture,
    );
  }

  static Future<void> useCase(String name, {String usage = ''}) async {
    final architecture = projectArchitecture();

    if (architecture != Architecture.cleanArchitecture) {
      throw UsageException(
        'Your project needs to be with Clean Architecture to run this command.',
        usage,
      );
    }

    await file_utils.createFile(
      'domain/usecases/$name',
      'use_case',
      templates.useCaseGenerator,
      generatorTest: templates.useCaseTestGenerator,
      architecture: architecture,
    );
  }
}
