import 'dart:async';

import 'package:args/command_runner.dart';
import 'package:flutter_snow_blower/flutter_snow_blower.dart';

class GeneratePageSubCommand extends CommandBase {
  @override
  final name = 'page';
  @override
  final description = 'Creates a page';

  GeneratePageSubCommand() {
    argParser.addFlag('controller',
        abbr: 'c',
        negatable: false,
        help: 'Creates a page without controller file');
    argParser.addFlag(
      'i18n',
      abbr: 'i',
      negatable: false,
      help: 'Creates a page without i18n file',
    );
  }

  @override
  Future<FutureOr<void>> run() async {
    if (argResults.rest.isEmpty) {
      throw UsageException('value not passed for a module command', usage);
    } else {
      await Generate.page(
        argResults.rest.first,
        argResults['controller'],
        i18nLess: argResults['i18n'],
      );
    }
    super.run();
  }
}

class GeneratePageAbbrSubCommand extends GeneratePageSubCommand {
  @override
  final name = 'p';
}
