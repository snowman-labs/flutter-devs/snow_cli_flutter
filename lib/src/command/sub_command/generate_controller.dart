import 'package:args/command_runner.dart';
import 'package:flutter_snow_blower/flutter_snow_blower.dart';

class GenerateControllerSubCommand extends CommandBase {
  @override
  final name = 'controller';
  @override
  final description = 'Creates a controller';

  GenerateControllerSubCommand() {
    argParser.addFlag(
      'notest',
      abbr: 'n',
      negatable: false,
      help: 'no create file test',
    );
  }

  @override
  Future<void> run() async {
    if (argResults.rest.isEmpty) {
      throw UsageException('value not passed for a module command', usage);
    } else {
      await Generate.controller(
        argResults.rest.first,
        'controller',
        haveTest: !argResults['notest'],
      );
    }
    super.run();
  }
}

class GenerateControllerAbbrSubCommand extends GenerateControllerSubCommand {
  @override
  final name = 'c';
}
