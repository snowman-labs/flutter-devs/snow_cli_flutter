import 'dart:async';

import 'package:args/command_runner.dart';
import 'package:flutter_snow_blower/flutter_snow_blower.dart';
import 'package:flutter_snow_blower/src/enums/architecture_enum.dart';
import 'package:flutter_snow_blower/src/enums/internationalization.dart';
import 'package:flutter_snow_blower/src/enums/persistence_enum.dart';

class CreateCommand extends CommandBase {
  @override
  final name = 'create';
  @override
  final description = 'Create a Flutter project with basic structure';
  @override
  final invocationSuffix = '<project name>';

  CreateCommand() {
    argParser.addFlag(
      'complete',
      abbr: 'c',
      negatable: false,
      help:
          'Create a complete flutter project with a sample project with login structure',
      defaultsTo: false,
    );

    argParser.addOption(
      'description',
      abbr: 'd',
      help:
          'The description to use for your new Flutter project. This string ends up in the pubspec.yaml file.',
      defaultsTo:
          'A new Flutter project. Created by Snowman Labs with Flutter Snow Blower',
    );

    argParser.addOption(
      'org',
      abbr: 'o',
      help:
          'The organization responsible for your new Flutter project, in reverse domain name notation. This string is used in Java package names and as prefix in the iOS bundle identifier.',
      defaultsTo: 'br.com.snowmanlabs',
    );

    argParser.addOption(
      'architecture',
      abbr: 'a',
      allowed: ['clean', 'mvc_modular'],
      help: 'Create a flutter project using an specified architecture.',
    );

    argParser.addOption(
      'persistence',
      abbr: 'p',
      allowed: ['hive', 'moor', 'hive_moor'],
      help: 'Set the project persistence library.',
    );

    argParser.addOption(
      'internationalization',
      abbr: 'i',
      allowed: ['i18n', 'flutterIntl'],
      help: 'Set the project internationalization library.',
    );

    argParser.addFlag(
      'use_enviroment',
      abbr: 'e',
      negatable: false,
      help:
          'Create a flutter project with 3 enviroment/flavors files(dev, qa and prod).',
      defaultsTo: true,
    );

    argParser.addFlag(
      'kotlin',
      abbr: 'k',
      negatable: false,
      help: 'use kotlin in Android project',
      defaultsTo: true,
    );

    argParser.addFlag(
      'swift',
      abbr: 's',
      negatable: false,
      help: 'use swift in ios project',
      defaultsTo: true,
    );

    argParser.addFlag(
      'androidx',
      abbr: 'x',
      negatable: false,
      help: 'use androidx on android project',
      defaultsTo: true,
    );
  }

  @override
  FutureOr<void> run() async {
    if (argResults.rest.isEmpty) {
      throw UsageException(
          'project name not passed for a create command', usage);
    } else {
      await create(
        projectName: argResults.rest.first,
        projectDescription: argResults['description'],
        projectOrg: argResults['org'],
        isKotlin: argResults['kotlin'],
        isSwift: argResults['swift'],
        architecture: ArchitectureMapper.fromString(argResults['architecture']),
        persistence: PersistenceMapper.fromString(argResults['persistence']),
        internationalization: InternationalizationMapper.fromString(
            argResults['internationalization']),
        complete: argResults['complete'],
        useEnvironment: argResults['use_enviroment'],
      );
    }
    super.run();
  }
}
