import 'dart:async';

import 'package:flutter_snow_blower/flutter_snow_blower.dart';
import 'package:flutter_snow_blower/src/enums/architecture_enum.dart';

class StartCommand extends CommandBase {
  @override
  final name = 'start';

  bool argsLength(int n) => argResults.arguments.length > n;
  @override
  final description =
      'Create a basic structure for your project (confirm that you have no data in the \"lib\" folder).';

  StartCommand() {
    argParser.addFlag('complete',
        abbr: 'c',
        negatable: false,
        help:
            'Create a complete flutter project with Themes separated of main.dart, named Routes and locales Strings configured');

    argParser.addOption(
      'architecture',
      abbr: 'a',
      allowed: ['clean', 'mvc_modular'],
      help: 'Create a flutter project using an specified architecture.',
    );

    argParser.addFlag('use_environment',
        abbr: 'e',
        negatable: false,
        help:
            'Create a flutter project with 3 environment/flavors files(dev, qa and prod).',
        defaultsTo: true);

    argParser.addFlag(
      'force',
      abbr: 'f',
      negatable: false,
      help: 'Erase lib dir',
    );
  }

  @override
  Future<FutureOr<void>> run() async {
    await start(
        completeStart: argResults['complete'],
        architecture: ArchitectureMapper.fromString(argResults['architecture']),
        force: argResults['force'],
        useEnvironment: argResults['use_environment']);
    super.run();
  }
}
