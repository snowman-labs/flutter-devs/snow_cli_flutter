import 'dart:io';

import 'package:flutter_snow_blower/src/enums/architecture_enum.dart';
import 'package:flutter_snow_blower/src/utils/pubspec.dart';
import 'package:flutter_snow_blower/src/utils/utils.dart';
import 'package:test/test.dart';

void main() {
  setUp(() {});

  group('ProjectArchitecture Finder test', () {
    test('Clean architecture is detected', () {
      mainDirectory = 'test/todo_app_clean/';
      expect(projectArchitecture(true), Architecture.cleanArchitecture);
      mainDirectory = 'test/todo_app_clean_with_src/';
      expect(projectArchitecture(true), Architecture.cleanArchitecture);
    });
    test('Modular is detected', () {
      mainDirectory = '';
      expect(projectArchitecture(true), Architecture.mvcModular);
    });
  });

  group('Exists File test', () {
    test('Exists di folder', () {
      mainDirectory = 'test/todo_app_clean/';
      expect(existsFile('di', true), true);
    });

    test('Exists di.dart', () {
      mainDirectory = 'test/todo_app_clean/';
      expect(existsFile('di/di.dart', true), true);
    });
  });

  group('FormatName Test', () {
    test('formatName without undeline', () {
      expect(formatName('home.dart'), 'Home');
    });

    test('formatName with undeline', () {
      expect(formatName('home_module.dart'), 'HomeModule');
    });
  });

  group('GetPubSpec Test', () {
    test('getPubSpec with directory', () async {
      expect(await getPubSpec(directory: Directory('')), isA<PubSpec>());
    });
    test('getPubSpec without directory', () async {
      expect(await getPubSpec(), isA<PubSpec>());
    });
  });

  group('GetNamePackage Test', () {
    test('getNamePackage snow_blower', () async {
      mainDirectory = '';
      expect(await getNamePackage(), 'flutter_snow_blower');
    });
    test('getNamePackage todo_app_clean', () async {
      mainDirectory = 'test/todo_app_clean/';
      expect(await getNamePackage(), 'todo_app_clean');
    });
  });

  group('LibPath Test', () {
    test('libPath 01', () async {
      mainDirectory = '';
      expect(await libPath('folder/app/home', reset: true),
          'lib/src/folder/app/home');
    });

    test('libPath 02', () async {
      mainDirectory = '';
      expect(await libPath('command', reset: true), 'lib/src/command');
    });
  });

  group('ValidateUrl Test', () {
    test('validateUrl 01', () async {
      expect(await validateUrl('https://github.com/flutterando'), true);
    });

    test('validateUrl 02', () async {
      expect(await validateUrl('http://github.com/flutterando'), true);
    });

    test('validateUrl 03', () async {
      expect(await validateUrl('github.com/flutterando'), false);
    });
  });
}
