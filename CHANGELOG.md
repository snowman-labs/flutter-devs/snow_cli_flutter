# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [2021.2.0] - 2021-04-26

- Updated create and generated templates to the new version of packages

## [2021.1.4] - 2021-04-26

- Fixed create command on flutter 2.0

## [2021.1.3] - 2021-01-19

### Added

- Support to new Flutter Modular version
- Prompt to choose what kind of Data Source you want to create

### Fixes

- Import errors and template errors when creating a new project

### Changed

- Method copyWith exported from Mappers to Entities

## [0.2.1] - 2020-11-02

### Fixes

- Fix: Invalid argument(s): Could not find an option named "internationalization".

## [0.2.0] - 2020-10-30

### Added

- Added support to flutter_intl

### Fixes

- Fixed error `-androidx` when start with flutter version > 1.23.0

## [0.1.1] - 2020-06-30

### Added

- Added support to new version of flutter_snow_base

## [0.1.0] - 2020-06-25

### Added

- Added example/README.md with the references of all the examples
- Added new file on start command, the validators. Is an util that you can centralize all the validators of the
application, and also your i18n file.

### Fixes

- Reformated the code with pedantic rules
- Fixed images of Readme
- Fixed .gitignore errors
- Fixed uninstall of packages with more than one line

## [0.0.1] - 2020-06-25

### Added

- Initial version of the package.
- Thanks to [Denis Viana](https://gitlab.com/denisviana) and to [Lucas Polazzo](https://gitlab.com/LucazzP) that made
  this first version of the package!

[Unreleased]: https://gitlab.com/snowman-labs/flutter-devs/snow_cli_flutter/-/compare/master...v0.2.0
[0.2.0]: https://gitlab.com/snowman-labs/flutter-devs/snow_cli_flutter/-/compare/v0.1.1...v0.2.0
[0.1.1]: https://gitlab.com/snowman-labs/flutter-devs/snow_cli_flutter/-/compare/v0.1.0...v0.1.1
[0.1.0]: https://gitlab.com/snowman-labs/flutter-devs/snow_cli_flutter/-/compare/v0.0.1...v0.1.0
[0.0.1]: https://gitlab.com/snowman-labs/flutter-devs/snow_cli_flutter/-/tags/v0.0.1
