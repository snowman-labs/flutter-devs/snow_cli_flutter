import 'package:flutter_modular_test/flutter_modular_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:i18n_extension/i18n_extension.dart';
import 'package:example_mvc_modular_complete/src/app_module.dart';
import 'package:example_mvc_modular_complete/src/modules/home/home_module.dart';
import 'package:example_mvc_modular_complete/src/modules/home/home_page.dart';
import 'package:example_mvc_modular_complete/src/modules/login/login_module.dart';
import 'package:example_mvc_modular_complete/src/modules/login/pages/login/login_page.dart';
import 'package:example_mvc_modular_complete/src/modules/login/pages/register/register_page.dart';

import '../../test_variants/flavors_test_variant.dart';

void main() {
  setUp(() {
    initModule(AppModule(), initialModule: true);
    initModule(LoginModule());
    initModule(HomeModule());
  });

  group('Login module i18n tests', () {
    testWidgets('Verify if the LoginPage have all translations', (tester) async {
      await tester.pumpWidget(buildTestableWidget(LoginPage()));
      expect(Translations.missingKeys, isEmpty);
      expect(Translations.missingTranslations, isEmpty);
    }, variant: FlavorsTestVariant());

    testWidgets('Verify if the RegisterPage have all translations', (tester) async {
      await tester.pumpWidget(buildTestableWidget(RegisterPage()));
      expect(Translations.missingKeys, isEmpty);
      expect(Translations.missingTranslations, isEmpty);
    }, variant: FlavorsTestVariant());
  });

  group('Home module i18n tests', () {
    testWidgets('Verify if the HomePage have all translations', (tester) async {
      await tester.pumpWidget(buildTestableWidget(HomePage()));
      expect(Translations.missingKeys, isEmpty);
      expect(Translations.missingTranslations, isEmpty);
    }, variant: FlavorsTestVariant());
  });
}
