import 'package:example_mvc_modular_complete/src/shared/auth/auth_store.dart';
import 'package:example_mvc_modular_complete/src/shared/models/user_model.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';

part 'register_controller.g.dart';

class RegisterController = _RegisterControllerBase with _$RegisterController;

abstract class _RegisterControllerBase with Store {
  final authStore = Modular.get<AuthStore>();

  @observable
  bool loading = false;

  @action
  Future<bool> registerWithEmail(String email, String password) async {
    UserModel user;
    try {
      loading = true;
      user = await authStore.registerWithEmail(email, password);
    } finally {
      loading = false;
    }
    return user != null;
  }
}
