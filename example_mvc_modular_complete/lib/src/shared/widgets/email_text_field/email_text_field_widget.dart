import 'package:flutter/material.dart';
import 'package:example_mvc_modular_complete/src/shared/helpers/validators.dart';

class EmailTextFieldWidget extends StatelessWidget {
  final TextEditingController controller;
  final void Function(String) onSaved;
  final String label;

  const EmailTextFieldWidget({
    Key key,
    this.controller,
    this.onSaved,
    this.label,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      onSaved: onSaved,
      validator: Validators.email,
      decoration: InputDecoration(labelText: label),
    );
  }
}
