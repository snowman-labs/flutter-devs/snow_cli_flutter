import 'dart:convert';

import 'package:flutter/foundation.dart';

@immutable
class UserModel {
  final String email;

  UserModel({
    this.email,
  });

  UserModel copyWith({
    String email,
  }) {
    return UserModel(
      email: email ?? this.email,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'email': email,
    };
  }

  static UserModel fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return UserModel(
      email: map['email'],
    );
  }

  String toJson() => json.encode(toMap());

  static UserModel fromJson(String source) => fromMap(json.decode(source));

  @override
  String toString() => 'UserModel(email: $email)';

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is UserModel && o.email == email;
  }

  @override
  int get hashCode => email.hashCode;
}
