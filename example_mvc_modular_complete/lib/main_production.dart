import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_snow_base/flutter_snow_base.dart';
import 'package:example_mvc_modular_complete/src/app_module.dart';
import 'package:example_mvc_modular_complete/src/shared/constants/constants.dart';
import 'package:example_mvc_modular_complete/src/shared/helpers/error_mapper.dart';

void main() {
  RunAppSnow(
    ModularApp(
      module: AppModule(),
    ),
    flavorValues: Constants.flavorProd,
    getItInit: () => Resource.setErrorMapper(ErrorMapper.from),
    flavor: Flavor.production,
  );
}
