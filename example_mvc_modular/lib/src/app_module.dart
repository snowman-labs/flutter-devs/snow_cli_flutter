import 'package:example_mvc_modular/src/shared/clients/hive_client.dart';
import 'package:example_mvc_modular/src/todo_repository.dart';
import 'package:example_mvc_modular/src/shared/clients/dio_client.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'app_widget.dart';
import 'modules/home/home_module.dart';

class AppModule extends Module {
  @override
  final List<Bind> binds = [
    Bind((i) => TodoRepository()),
    Bind((i) => DioClient()),
    Bind((i) => HiveClient()),
  ];

  @override
  final List<ModularRoute> routes = [
    ModuleRoute(Modular.initialRoute, module: HomeModule()),
  ];
}
