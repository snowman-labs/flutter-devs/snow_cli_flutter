import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_snow_base/flutter_snow_base.dart';
import 'package:mobx/mobx.dart';
import 'package:example_mvc_modular/src/shared/clients/dio_client.dart';
import 'package:example_mvc_modular/app_flavor_values.dart';

void main() {
  DioClient dio;

  setUp(() {
    FlavorConfig(
      flavor: Flavor.production,
      values: AppFlavorValues(
        baseUrl: "",
        features: null,
      ),
    );
    dio = DioClient();
  });

  test('Dio client test', () async {
    final resObservable = dio.get("wrongPath").asObservable();
    expect(resObservable, isNotNull);
    expect(resObservable.status, FutureStatus.pending);
    final res = await resObservable.catchError((e) {});
    expect(resObservable.status, FutureStatus.rejected);
    expect(res, isNull);
  });
}
