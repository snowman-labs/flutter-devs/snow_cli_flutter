#### Checklist de Revisão de Banco de Dados

Obrigado por se tornar um revisor de ~"database"! Por favor, 
trabalhe na lista a seguir para completar sua configuração. Para qualquer pergunta,
procure mais informações no canal do slack #dev.

- [ ] Mude o titulo da issue para incluir seu nome: `Checklist de Revisão de Banco de Dados: Seu nome`
- [ ] Revisão geral do [guia de code review](https://docs.gitlab.com/ee/development/code_review.html)
- [ ] Revisão do [guia de revisão de banco de dados](https://about.gitlab.com/handbook/engineering/workflow/code-review/database.html)
- [ ] Leia o [handbook de estilos de migração de banco de dados](https://docs.gitlab.com/ee/development/migration_style_guide.html)
- [ ] Familiarize-se com as melhores praticas no [guia de banco de dados](https://docs.gitlab.com/ee/development/#database-guides)
- [ ] Assista o video [Otimizando Consultas de Banco de dados em Rails: Episodio 1](https://www.youtube.com/watch?v=79GurlaxhsI)(english)
- [ ] Leia [Entendendo esquema do EXPLAIN](https://docs.gitlab.com/ee/development/understanding_explain_plans.html)
- [ ] Revise [melhores praticas de banco de dados](https://docs.gitlab.com/ee/development/#best-practices)
- [ ] Revise como o Gitlab [restaura instancias do banco de dados do backup](https://ops.gitlab.net/gitlab-com/gl-infra/gitlab-restore/postgres-gprd) para testes e para ter certeza que você for executou a pipeline (verifique [README.md](https://ops.gitlab.net/gitlab-com/gl-infra/gitlab-restore/postgres-gprd/blob/master/README.md))
- [ ] Tenha certeza que você acessou a última replica somente-leitura no respectivo ambiente

Note que *aprovando e aceitando* merge requests é *restrito* somente 
para Maintainers de Banco de dados. Como um revisor, passe o MRs para o 
maintainer aprovar.

###### Onde ir para tirar duvidas?

Vá para o canal do Slack #dev e filtre por #database.

/label ~meta ~database
