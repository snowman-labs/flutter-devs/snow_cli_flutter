<!---
Por favor, leia isso!

Antes de abrir uma nova issue, tenha certeza de buscar pelas palavras-chave
nas issues filtrador pela label "bug"

Alem disso, verifique se a issue que está enviando não seja duplicada.
--->

### Resumo

(Resuma o bug encontrado de forma concisa)

### Passos para reproduzir

(Como reproduzir o problema - isto é muito importante)

### Exemplos

(Por favor, informe exemplos que usou para reproduzir o erro)

### Qual o atual comportamento do *bug*?

(O que acontece atualmente)

### Qual o comportamento *correto* esperado?

(O que você deveria ver)

### Logs e/ou Screenshots relevantes

(Cole qualquer log relevante aqui - por favor use o bloco de código (```) para formatar 
a saida de console, logs e código, senão a leitura será muito dificil)

### Onde verificar

(Informe onde o erro ocorreu ou está ocorrendo. Informe versão, url, ambiente, e relacionados)

### Possiveis soluções

(Se você puder, link para a linha do código que pode ser responsavel pelo problema)

/label ~bug
