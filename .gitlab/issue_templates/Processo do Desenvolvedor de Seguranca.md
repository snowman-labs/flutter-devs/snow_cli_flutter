### Antes de iniciar o trabalho de lançamento de segurança

- [ ] Leia o [processo de segurança para desenvolvedores] se você não está familiarizado com isso.
- [ ] Link para a issue original adicionado para esse a [seção de links](#links)
- [ ] Execute os scripts de analise de segurança do projeto, veja com o Tech Lead ou Maintainer sobre.
- [ ] Crie uma branch com o prefixo `security-`
- [ ] Crie um MR (Merge Request) para a devida branch, exemplo `master`

#### Backports

- [ ] Uma vez que o MR está pronto para ser mergeado, crie MRs para os últimos 3 releases (caso existam)
- [ ] Tenha certeza que todos MRs tenham link na [seção de links](#links)

#### Documentação e detalhes finais

- [ ] Verifique o canal de #qa filtrando pela tag #security e saiba mais sobre. Adicione o link, caso necessário, na [seção de links](#links)
- [ ] Adicione links para esta issue e seus MRs na descrição da issue de release de segurança
- [ ] Encontre as versões afetadas (o historico do git de arquivos efetados podem ajudar com isso) e adicione-os na [seção de detalhes](#detalhes)
- [ ] Informe qualquer nota de upgrade que o usuário possa precisar para sua conta na [seção de detalhes](#detalhes)
- [ ] Adicione Yes/No (Sim/Não) e mais detalhes se necessário para a migraçao e configuração nas colunas na [seção de detalhes](#detalhes)
- [ ] Uma vez que sua MR esteja mergeada na `master`, comente na issue original com o link indicando que foi resolvido.

### Resumo

#### Links

| Descrição | Link |
| -------- | -------- |
| Issue Original   | #TODO  |
| Issue de Security Release   | #TODO  |
| `master` MR | !TODO   |
| `Backport X.Y` MR | !TODO   |
| `Backport X.Y` MR | !TODO   |
| `Backport X.Y` MR | !TODO   |

#### Detalhes

| Descrição | Detalhes | Mais detalhes|
| -------- | -------- | -------- |
| Versões afetadas | X.Y  | |
| Notas de Upgrade | | |
| Configurações do GitLab atualizada | Yes/No| |
| Necessária Migração | Yes/No | |

[security process for developers]: https://gitlab.com/gitlab-org/release/docs/blob/master/general/security/developer.md
[RM list]:  https://about.gitlab.com/release-managers/

/label ~security
