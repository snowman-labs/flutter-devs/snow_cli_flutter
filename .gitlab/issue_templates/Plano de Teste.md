# Plano de Teste

<!-- Esta issue traça as atividades relacionadas ao teste para uma issue ou epic.

[Aqui tem um plano de teste de exemplo](https://gitlab.com/gitlab-org/gitlab-ce/issues/50353)

Este e outros comentários precisam ser removidos quando você escrever o plano -->

## Introdução

<!-- Breve esboço do que vai ser testado

Mencione na issue(s) este plano de teste está relaciona a -->

## Escopo

<!-- Indique quaisquer limites nos aspectos da feature a ser testada
Trace os tipos de dados a serem inclusos
Trace os tipos de testes a serem performados (funcional, segurança, performance, 
banco de dados, automatizado, etc) -->

## ACC Matrix (Matriz de Atributos, Componentes e Competencias)

<!--  Use a Matriz a seguir como template para identificar os Atributos, Componentes e
Competencias, relevantes para o escopo deste plano de testes. Adicione ou remova
Atributos e Componentes como for necessário e liste as Competencias na próxima seção:

Atributos (colunas) são advérbios ou adjetivos que descrevem (em alto nivel)
as qualidades que os Componentes precisam garantir nos testes.

Componentes (linhas) são substantivos que definem as maiores partes do produto a ser testado.

Competencias ligam Atributos à Componentes. Eles são o que seu produto precisa fazer
para garantir que o Componente possua um Atributo.

Para mais informações veja o [Google Testing Blog artigo sobre o plano de teste 
de 10 minutos](https://testing.googleblog.com/2011/09/10-minute-test-plan.html) 
e [esta página wiki de uma ferramenta de código aberto que implementa 
o modelo de ACC](https://code.google.com/archive/p/test-analytics/wikis/AccExplained.wiki). -->

|            | Seguro | Responsivo | Intuitivo | Confiável|
|------------|:------:|:----------:|:---------:|:--------:|
| Admin      |        |            |           |          |
| Grupos     |        |            |           |          |
| Projeto    |        |            |           |          |
| Repositorio|        |            |           |          |
| Issues     |        |            |           |          |
| MRs        |        |            |           |          |
| CI/CD      |        |            |           |          |
| Ops        |        |            |           |          |
| Registro   |        |            |           |          |
| Wiki       |        |            |           |          |
| Snippets   |        |            |           |          |
| Configs    |        |            |           |          |
| Tracking   |        |            |           |          |
| API        |        |            |           |          |

## Competencias

<!-- Use a Matriz ACC acima para ajudar você a identificar Competencias de cada intercessão
relevante dos Componentes e Atributos.

Algumas funcionalidades pode ser simples o bastante que elas envolvam somente um componente,
enquanto que funcionalidades mais complexas envolvam multiplas ou todas as elas.

Exemplo (de https://gitlab.com/gitlab-org/gitlab-ce/issues/50353):
* Repositorio é 
  * Intuitivo
    * Ser facil de selecionar o template desejado 
    * Nãso ser obrigatório ações desnecessárias para salvar as mudanças
    * Ser fácil apra desfazer as mudanças depois de selecionar um template
  * Responsivo
    * A lista de templates pode ser restrita para permitir um usuário encontrar um template especifico dentre vários
    * Uma vez selecionado o template o conteúdo do arquivo atualiza rapidamente e suavemente. 
-->

## Plano de Teste

<!-- Se o escopo é pequeno o bastante você pode precisar escrever uma lista de 
testes para performar. Isto pode ser usado para guiar seus testes de Competencias.

Se a funcionalidade é mais complexa, especialmente se envolve multiplos Componentes,
trace brevemente uma trilha de testes aqui. Quando identificar testes para performar 
tenha certeza de considerar o risco. Observe os niveis de risco inerentes/conhecidos
para que os testes possam se concentrar nas areas de maior risco, primeiramente. 

Novo teste ponta a ponta e de integração (Selenium e API) precisam ser adicionados
para a **planilha de teste de cobertura** (a ser definido)

Por favor, informe se os testes automatizados já existirem.

Quando adicionar novos testes automatizados, por favor, 
tenha em mente os [niveis de teste](https://docs.gitlab.com/ce/development/testing_guide/testing_levels.html).
-->

/label ~qa ~"test\-plan"
