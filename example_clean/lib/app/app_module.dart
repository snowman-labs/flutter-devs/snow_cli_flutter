import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:example_clean/app/pages/home/home_module.dart';

import 'app_widget.dart';

class AppModule extends Module {
  // here will be any class you want to inject into your project (eg bloc, dependency)
  @override
  final List<Bind> binds = [];

  // here will be the routes of your module
  @override
  final List<ModularRoute> routes = [
    ModuleRoute(
      '/',
      module: HomeModule(),
    ),
  ];
}
