This package provides multiple examples, according to the selected architecture and if you selected the complete mode.

- [Clean Architecture Example](https://gitlab.com/snowman-labs/flutter-devs/snow_cli_flutter/-/tree/master/example_clean)
- [Clean Architecture Example (complete mode)](https://gitlab.com/snowman-labs/flutter-devs/snow_cli_flutter/-/tree/master/example_clean_complete)
- [MVC modular Example](https://gitlab.com/snowman-labs/flutter-devs/snow_cli_flutter/-/tree/master/example_mvc_modular)
- [MVC modular Example (complete mode)](https://gitlab.com/snowman-labs/flutter-devs/snow_cli_flutter/-/tree/master/example_mvc_modular_complete)
