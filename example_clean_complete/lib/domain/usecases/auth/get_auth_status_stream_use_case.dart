import 'dart:async';

import 'package:injectable/injectable.dart';
import 'package:example_clean_complete/domain/entities/auth_status.dart';
import 'package:example_clean_complete/domain/entities/user_entity.dart';
import 'package:example_clean_complete/domain/repositories/auth/auth_repository.dart';
import 'package:example_clean_complete/domain/usecases/base/base_stream_use_case.dart';

@singleton
class GetAuthStatusStreamUseCase extends BaseStreamUseCase<void, AuthStatus> {
  final AuthRepository _repository;

  GetAuthStatusStreamUseCase(this._repository) {
    stream = _repository.getUserStream().transform(
      StreamTransformer<UserEntity, AuthStatus>.fromHandlers(
        handleData: (user, sink) {
          sink.add(user == null ? AuthStatus.logoff : AuthStatus.login);
        },
      ),
    );
    _repository.getUser();
  }

  Stream<AuthStatus> stream;

  @override
  Stream<AuthStatus> call([void params]) {
    return stream;
  }
}
