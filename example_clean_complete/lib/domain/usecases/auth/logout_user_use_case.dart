import 'package:injectable/injectable.dart';
import 'package:example_clean_complete/domain/repositories/auth/auth_repository.dart';
import 'package:example_clean_complete/domain/usecases/base/base_future_use_case.dart';

@injectable
class LogoutUserUseCase extends BaseFutureUseCase<void, void> {
  final AuthRepository _repository;

  LogoutUserUseCase(this._repository);

  @override
  Future<void> call([void params]) => _repository.logoutUser();
}
