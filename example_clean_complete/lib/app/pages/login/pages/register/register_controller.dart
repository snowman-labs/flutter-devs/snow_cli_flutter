import 'package:mobx/mobx.dart';
import 'package:example_clean_complete/di/di.dart';
import 'package:example_clean_complete/domain/entities/auth_entity.dart';
import 'package:example_clean_complete/domain/entities/user_entity.dart';
import 'package:example_clean_complete/domain/usecases/auth/register_user_email_use_case.dart';

part 'register_controller.g.dart';

class RegisterController = _RegisterControllerBase with _$RegisterController;

abstract class _RegisterControllerBase with Store {
  final registerUserEmail = getIt.get<RegisterUserEmailUseCase>();

  @observable
  bool loading = false;

  @action
  Future<bool> registerWithEmail(String email, String password) async {
    UserEntity user;
    try {
      loading = true;
      user =
          await registerUserEmail(AuthEntity(email: email, password: password));
    } finally {
      loading = false;
    }
    return user != null;
  }
}
