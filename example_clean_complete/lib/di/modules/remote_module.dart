import 'package:firebase_auth/firebase_auth.dart';
import 'package:injectable/injectable.dart';

// Use this module to inject your third-party dependencies like [FirebaseAuth]
// E.g. FirebaseAuth get firebaseAuth => FirebaseAuth.instance;

@module
abstract class RemoteModule {
  FirebaseAuth get firebaseAuth => FirebaseAuth.instance;
}
