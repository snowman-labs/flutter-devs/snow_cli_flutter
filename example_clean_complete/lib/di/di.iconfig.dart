// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:example_clean_complete/data/remote/dio_client.dart';
import 'package:example_clean_complete/di/modules/remote_module.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:example_clean_complete/di/modules/local_module.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:example_clean_complete/data/local/hive_client.dart';
import 'package:example_clean_complete/data/data_sources/auth/auth_local_data_source.dart';
import 'package:example_clean_complete/data/data_sources/auth/auth_remote_data_source.dart';
import 'package:example_clean_complete/data/repositories/auth/auth_repository_impl.dart';
import 'package:example_clean_complete/domain/repositories/auth/auth_repository.dart';
import 'package:example_clean_complete/domain/usecases/auth/get_auth_status_stream_use_case.dart';
import 'package:example_clean_complete/domain/usecases/auth/get_user_stream_use_case.dart';
import 'package:example_clean_complete/domain/usecases/auth/get_user_use_case.dart';
import 'package:example_clean_complete/domain/usecases/auth/login_user_email_use_case.dart';
import 'package:example_clean_complete/domain/usecases/auth/logout_user_use_case.dart';
import 'package:example_clean_complete/domain/usecases/auth/register_user_email_use_case.dart';
import 'package:get_it/get_it.dart';

void $initGetIt(GetIt g, {String environment}) {
  final remoteModule = _$RemoteModule();
  final localModule = _$LocalModule();
  g.registerFactory<FirebaseAuth>(() => remoteModule.firebaseAuth);
  g.registerFactory<AuthLocalDataSource>(
      () => AuthLocalDataSource(g<FlutterSecureStorage>()));
  g.registerFactory<AuthRemoteDataSource>(
      () => AuthRemoteDataSource(g<FirebaseAuth>()));
  g.registerFactory<GetUserStreamUseCase>(
      () => GetUserStreamUseCase(g<AuthRepository>()));
  g.registerFactory<GetUserUseCase>(() => GetUserUseCase(g<AuthRepository>()));
  g.registerFactory<LoginUserEmailUseCase>(
      () => LoginUserEmailUseCase(g<AuthRepository>()));
  g.registerFactory<LogoutUserUseCase>(
      () => LogoutUserUseCase(g<AuthRepository>()));
  g.registerFactory<RegisterUserEmailUseCase>(
      () => RegisterUserEmailUseCase(g<AuthRepository>()));

  //Eager singletons must be registered in the right order
  g.registerSingleton<DioClient>(DioClient());
  g.registerSingleton<FlutterSecureStorage>(localModule.secureStorage);
  g.registerSingleton<HiveClient>(HiveClient());
  g.registerSingleton<AuthRepository>(
      AuthRepositoryImpl(g<AuthRemoteDataSource>(), g<AuthLocalDataSource>()));
  g.registerSingleton<GetAuthStatusStreamUseCase>(
      GetAuthStatusStreamUseCase(g<AuthRepository>()));
}

class _$RemoteModule extends RemoteModule {}

class _$LocalModule extends LocalModule {}
