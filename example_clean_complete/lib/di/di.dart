import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';

import 'di.iconfig.dart';

final GetIt getIt = GetIt.instance;
bool _diInitialized = false;

@injectableInit
void configureInjection([String env]) {
  WidgetsFlutterBinding.ensureInitialized();
  if (!_diInitialized) {
    $initGetIt(getIt, environment: env);
    _diInitialized = true;
  }
}
