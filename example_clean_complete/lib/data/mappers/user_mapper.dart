import 'dart:convert';
import 'package:example_clean_complete/domain/entities/user_entity.dart';

/// Extension that map your [Entity] object to/from [Map], [Json] and other types if necessary.
/// E.g. With you using Moor as database you need map your [UserEntity] to [UserEntityDataTable].
/// So you can create a extension toTable/fromTable to map your data.
/// Every [Repository], [LocalDataSource], [RemoteDataSource] has a import of mappers.
/// To more info about extensions please visit: https://dart.dev/guides/language/extension-methods

extension UserMapper on UserEntity {
  copyWith({
    String email,
  }) {
    return UserEntity(
      email: email ?? this.email,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'email': email,
    };
  }

  UserEntity fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return UserEntity(
      email: map['email'],
    );
  }

  String toJson() => json.encode(toMap());

  UserEntity fromJson(String source) => fromMap(json.decode(source));
}
